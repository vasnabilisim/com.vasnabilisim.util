package com.vasnabilisim.jdbc;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.vasnabilisim.util.KeyValue;

/**
 * DataSourceInfoTester. 
 * Used to test datasources and drivers. 
 * @author Menderes Fatih GUVEN
 */
public class DataSourceInfoTester {

	/**
	 * Source. 
	 * Handles operations over this data source info.
	 */
	private DataSourceInfo dataSourceInfo;
	
	private Connection connection = null;
	private Statement statement = null;
	private ResultSet resultSet = null;

	/**
	 * Disposes the result set.
	 */
	public void disposeResultSet() {
		if(resultSet != null)
			try {resultSet.close();}catch(SQLException e){}
	}

	/**
	 * Disposes the statement.
	 */
	public void disposeStatement() {
		disposeResultSet();
		if(statement != null)
			try {statement.close();}catch(SQLException e){}
	}

	/**
	 * Disposes the connection.
	 */
	public void disposeConnection() {
		disposeStatement();
		if(connection != null)
			try {connection.close();}catch(SQLException e){}
	}
	
	/**
	 * Disposes the handler.
	 */
	public void dispose() {
		disposeConnection();
	}
	
	/**
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		disposeConnection();
		super.finalize();
	}
	
	/**
	 * Source constructor. 
	 * Handles operations over given data source info.
	 * @param dataSourceInfo
	 */
	public DataSourceInfoTester(DataSourceInfo dataSourceInfo) {
		this.dataSourceInfo = dataSourceInfo;
	}
	
	/**
	 * Tests the driver of the datasource.
	 * @throws
	 */
	public void testDriver() throws SQLException {
		testDriver(dataSourceInfo.getDriverInfo());
	}
	
	/**
	 * Tests given driver info.
	 * @param driverInfo
	 * @throws
	 */
	public void testDriver(DriverInfo driverInfo) throws SQLException {
		try {
			com.vasnabilisim.util.ClassLoader.loadClass(driverInfo.getDriverClass());
		}
		catch(ClassNotFoundException e) {
			throw new SQLException(String.format("Unable to find driver [%s].", driverInfo.getDriverClass()), e);
		}
	}
	
	/**
	 * Creates a sql driver from given info.
	 * @param driverInfo
	 * @return
	 * @throws
	 */
	public Driver createDriver(DriverInfo driverInfo) throws SQLException {
		try {
			@SuppressWarnings("unchecked")
			Class<? extends Driver> driverClass = (Class<? extends Driver>)com.vasnabilisim.util.ClassLoader.loadClass(driverInfo.getDriverClass());
			Driver driver = driverClass.newInstance();
			return driver;
		}
		catch(ClassNotFoundException e) {
			throw new SQLException(String.format("Unable to find driver [%s].", driverInfo.getDriverClass()), e);
		} 
		catch (InstantiationException | IllegalAccessException e) {
			throw new SQLException(String.format("Unable to create driver [%s].", driverInfo.getDriverClass()), e);
		} 
	}
	
	/**
	 * Returns a connection to the datasource.
	 * @return
	 * @throws
	 */
	public void createConnection() throws SQLException {
		disposeConnection();
		Driver driver = createDriver(dataSourceInfo.getDriverInfo());
		Properties properties = new Properties();
		properties.put("user", dataSourceInfo.getUsername());
		properties.put("password", dataSourceInfo.getPassword());
		for(KeyValue keyValue : dataSourceInfo.getPropertyList())
			properties.put(keyValue.getKey(), keyValue.getValue());
		try {
			connection = driver.connect(dataSourceInfo.getDataSourceUrl(), properties);
		} catch (SQLException e) {
			throw new SQLException(
					String.format(
							"Unable to connect datasource; [%s]\nPossible reason: %s", 
							dataSourceInfo.getDataSourceUrl(), 
							e.getMessage()
					), 
					e
			);
		}
	}
	
	/**
	 * Tests the datasource.
	 * @throws
	 */
	public void testDataSource() throws SQLException {
		createConnection();
		disposeConnection();
	}
	
	/**
	 * Executes given sql in the datasource.
	 * @param sql
	 * @throws
	 */
	public void executeQuery(String sql) throws SQLException {
		createConnection();
		statement = null;
		try {
			statement = connection.createStatement();
		}
		catch(SQLException e) {
			disposeConnection();
			throw new SQLException(String.format("Unable to execute query [%s].", sql), e);
		}
		try {
			resultSet = statement.executeQuery(sql);
		}
		catch(SQLException e) {
			disposeConnection();
			throw new SQLException(String.format("Unable to execute query [%s].", sql), e);
		}
	}
	
	/**
	 * Shifts to the next row in the result set.
	 * @return
	 * @throws
	 */
	public boolean nextResult() throws SQLException {
		if(resultSet == null)
			return false;
		try {
			return resultSet.next();
		}
		catch(SQLException e) {
			disposeConnection();
			throw new SQLException("Unable to navigate result set.", e);
		}
	}
	
	/**
	 * Is given named column exists in the result set.
	 * @param column
	 * @return
	 */
	public boolean isColumn(String column) {
		if(resultSet == null)
			return false;
		if(column == null)
			return false;
		try {
			resultSet.findColumn(column);
		}
		catch(SQLException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Returns the current value of given column from result set.
	 * @param column
	 * @return
	 * @throws
	 */
	public Object getColumn(String column) throws SQLException {
		if(resultSet == null)
			return false;
		if(column == null)
			return false;
		try {
			return resultSet.getObject(column);
		}
		catch(SQLException e) {
			throw new SQLException(String.format("Unable to find column [%s].", column), e);
		}
	}
}
