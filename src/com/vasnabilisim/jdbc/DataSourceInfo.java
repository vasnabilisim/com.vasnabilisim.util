package com.vasnabilisim.jdbc;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import com.vasnabilisim.util.CompareToBuilder;
import com.vasnabilisim.util.EqualsBuilder;
import com.vasnabilisim.util.KeyValue;

/**
 * Data Source Info class.
 * @author Menderes Fatih GUVEN
 */
public class DataSourceInfo implements Comparable<DataSourceInfo>, com.vasnabilisim.util.Cloneable<DataSourceInfo>, java.lang.Cloneable, Serializable {
	private static final long serialVersionUID = 6023295161848427963L;

	/**
	 * Parent.
	 */
	private DriverInfo driverInfo = null;
	
	/**
	 * Name of the datasource.
	 */
	private String datasourceName = null;
	
	/**
	 * Jdbc url extention of the datasource.
	 */
	private String datasourceUrl = null;
	
	/**
	 * User key to connect to datasource.
	 */
	private String username = null;
	
	/**
	 * Password to connect to datasource.
	 */
	private String password = null;
	
	/**
	 * Information about datasource.
	 */
	private String information = null;
	
	/**
	 * Properties.
	 */
	private List<KeyValue> propertyList = new ArrayList<>();
	
	/**
	 * Default constructor.
	 */
	public DataSourceInfo() {
	}

	/**
	 * Value constructor.
	 * @param id
	 * @param datasourceName
	 * @param datasourceUrl
	 * @param portNumber
	 * @param username
	 * @param password
	 * @param information
	 * @param databaseName
	 */
	public DataSourceInfo(	String datasourceName, 
							String datasourceUrl, 
							String username, 
							String password,
							String information) 
	{
		this.datasourceName = datasourceName;
		this.datasourceUrl = datasourceUrl;
		this.username = username;
		this.password = password;
		this.information = information;
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	public DataSourceInfo(DataSourceInfo source) {
		this.datasourceName = source.datasourceName;
		this.datasourceUrl = source.datasourceUrl;
		this.username = source.username;
		this.password = source.password;
		this.information = source.information;
		for(KeyValue keyValue : source.propertyList)
			this.propertyList.add(keyValue.cloneObject());
	}
	
	/**
	 * Returns the driver info.
	 * @return
	 */
	public DriverInfo getDriverInfo() {
		return driverInfo;
	}

	/**
	 * Sets the driver info.
	 * @param driverInfo
	 */
	public void setDriverInfo(DriverInfo driverInfo) {
		this.driverInfo = driverInfo;
	}

	/**
	 * Returns the password.
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Returns the username.
	 * @return
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Returns the datasource key.
	 * @return
	 */
	public String getDataSourceName() {
		return datasourceName;
	}

	/**
	 * Sets the datasource key.
	 * @param datasourceName
	 */
	public void setDataSourceName(String datasourceName) {
		this.datasourceName = datasourceName;
	}

	/**
	 * Returns the datasource url.
	 * @return
	 */
	public String getDataSourceUrl() {
		return datasourceUrl;
	}

	/**
	 * Sets the datasource url.
	 * @param datasourceUrl
	 */
	public void setDataSourceUrl(String datasourceUrl) {
		this.datasourceUrl = datasourceUrl;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return datasourceName;
	}
	
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj == this)
			return true;
		if(!(obj instanceof DataSourceInfo))
			return false;
		DataSourceInfo other = (DataSourceInfo)obj;
		return new EqualsBuilder().appendNotNull(this.datasourceUrl, other.datasourceUrl).toBoolean();
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected Object clone() {
		return cloneObject();
	}

	/**
	 * @see com.vasnabilisim.util.Cloneable#cloneObject()
	 */
	@Override
	public DataSourceInfo cloneObject() {
		return new DataSourceInfo(this);
	}

	/**
	 * @see java.lang.Comparable#compareTo(T)
	 */
	public int compareTo(DataSourceInfo other) {
		if(other == null)
			return 1;
		if(this == other)
			return 0;
		return new CompareToBuilder().append(this.datasourceName, other.datasourceName).toInt();
	}

	/**
	 * Returns properties.
	 * @return
	 */
	public List<KeyValue> getPropertyList() {
		return propertyList;
	}

	/**
	 * Set properties
	 * @param propertyList
	 */
	public void setPropertyList(List<KeyValue> propertyList) {
		this.propertyList = propertyList;
	}
	
	/**
	 * Adds a property to datasource.
	 * @param key
	 * @param value
	 */
	public void addProperty(String name, String value) {
		addProperty(new KeyValue(name, value));
	}

	/**
	 * Adds given property to datasource.
	 * @param keyValue
	 */
	public void addProperty(KeyValue keyValue) {
		propertyList.add(keyValue);
		Collections.sort(propertyList);
	}
	
	/**
	 * Removes given property.
	 * @param keyValue
	 */
	public void removeProperty(KeyValue keyValue) {
		propertyList.remove(keyValue);
	}
	
	/**
	 * Returns the information.
	 * @return
	 */
	public String getInformation() {
		return information;
	}
	
	/**
	 * Sets the information.
	 * @param information
	 */
	public void setInformation(String information) {
		this.information = information;
	}
	
	/**
	 * Create a sql connection from this.
	 * @return
	 * @throws SQLException
	 */
	public Connection createConnection() throws SQLException {
		Driver driver = driverInfo.createDriver();
		Properties properties = new Properties();
		properties.put("user", username);
		properties.put("password", password);
		for(KeyValue property : propertyList)
			properties.put(property.getKey(), property.getValue());
		try {
			Connection connection = driver.connect(datasourceUrl, properties);
			//connection.setAutoCommit(false);
			return connection;
		}
		catch(SQLException e) {
			String message = String.format("Unable to connect datasource %s. Possible reason: %s.", 
					datasourceUrl, 
					e.getMessage());
			throw new SQLException(message, e);
		}
	}
}
