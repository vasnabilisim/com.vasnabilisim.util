package com.vasnabilisim.jdbc;

import java.io.File;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.vasnabilisim.xml.XmlException;
import com.vasnabilisim.xmlmodel.XmlModel;

/**
 * DriverInfos
 * 
 * @author Menderes Fatih GUVEN
 */
public class DriverInfos implements Serializable {
	private static final long serialVersionUID = 6956641757163778212L;

	private List<DriverInfo> driverInfoList = new ArrayList<>();
	
	public DriverInfos() {
	}
	
	@Override
	public String toString() {
		return "DriverInfos";
	}

	/**
	 * Returns all driver infos.
	 * @return
	 */
	public List<DriverInfo> getDriverInfoList() {
		return Collections.unmodifiableList(driverInfoList);
	}
	
	/**
	 * Returns the given named driver info.
	 * @param name
	 * @return
	 */
	public DriverInfo getDriverInfo(String name) {
		for(DriverInfo driverInfo : driverInfoList) {
			if(driverInfo.getDriverName().equals(name))
				return driverInfo;
		}
		return null;
	}
	
	/**
	 * Returns given named data source info from given named driver info.
	 * @param driverName
	 * @param dataSourceName
	 * @return
	 */
	public DataSourceInfo getDataSourceInfo(String driverName, String dataSourceName) {
		DriverInfo driverInfo = getDriverInfo(driverName);
		if(driverInfo == null)
			return null;
		return driverInfo.getDataSourceInfo(dataSourceName);
	}
	
	/**
	 * Adds given driver info.
	 * @param driverInfo
	 * @return
	 */
	public boolean addDriverInfo(DriverInfo driverInfo) {
		return driverInfoList.add(driverInfo);
	}
	
	/**
	 * Removes given driver info.
	 * @param driverInfo
	 * @return
	 */
	public boolean removeDriverInfo(DriverInfo driverInfo) {
		return driverInfoList.remove(driverInfo);
	}
	
	private static DriverInfos instance = null;
	public static DriverInfos getInstance() {
		return instance;
	}
	public static void load(String fileName) throws SQLException {
		try {
			instance = (DriverInfos) XmlModel.parse(DriverInfos.class.getResourceAsStream("DataSource.xmlmodel")).decode(new File(fileName));
		} catch (XmlException e) {
			new SQLException(e);
		}
	}
}
