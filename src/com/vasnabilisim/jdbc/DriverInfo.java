package com.vasnabilisim.jdbc;

import java.io.Serializable;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.vasnabilisim.util.CompareToBuilder;
import com.vasnabilisim.util.EqualsBuilder;

/**
 * JDBC Driver info class.
 * @author Menderes Fatih GUVEN
 */
public class DriverInfo implements Comparable<DriverInfo>, com.vasnabilisim.util.Cloneable<DriverInfo>, java.lang.Cloneable, Serializable {
	private static final long serialVersionUID = -6153155220996267249L;

	/**
	 * Name of the driver.
	 */
	private String driverName = null;
	
	/**
	 * Full class path of the driver.
	 */
	private String driverClass = null;
	
	/**
	 * Jdbc url of the driver.
	 */
	private String driverUrl = null;
	
	/**
	 * Information about driver.
	 */
	private String information = null;
	
	/**
	 * Child data sources.
	 */
	List<DataSourceInfo> dataSourceInfoList = new ArrayList<>();
	
	/**
	 * Default constructor.
	 */
	public DriverInfo() {
	}

	/**
	 * Value constructor.
	 * @param driverName
	 * @param driverClass
	 * @param driverUrl
	 * @param information
	 */
	public DriverInfo(String driverName, String driverClass, String driverUrl, String information) {
		this.driverName = driverName;
		this.driverClass = driverClass;
		this.driverUrl = driverUrl;
		this.information = information;
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	public DriverInfo(DriverInfo source) {
		this.driverName = source.driverName;
		this.driverClass = source.driverClass;
		this.driverUrl = source.driverUrl;
		this.information = source.information;
	}
	
	/**
	 * Returns the driver key.
	 * @return
	 */
	public String getDriverName() {
		return driverName;
	}

	/**
	 * Sets the driver key.
	 * @param driverName
	 */
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	/**
	 * Returns the full class path of the driver.
	 * @return
	 */
	public String getDriverClass() {
		return driverClass;
	}

	/**
	 * Sets the full class path of the driver.
	 * @param driverClass
	 */
	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}
	
	/**
	 * Returns the url of the driver.
	 * @return
	 */
	public String getDriverUrl() {
		return driverUrl;
	}

	/**
	 * Sets the url of the driver.
	 * @param driverUrl
	 */
	public void setDriverUrl(String driverUrl) {
		this.driverUrl = driverUrl;
	}
	
	/**
	 * Returns the information.
	 * @return
	 */
	public String getInformation() {
		return information;
	}
	
	/**
	 * Sets the information.
	 * @param information
	 */
	public void setInformation(String information) {
		this.information = information;
	}

	/**
	 * Returns the list of data source infos contained within this driver info.
	 * @return
	 */
	public List<DataSourceInfo> getDataSourceInfoList() {
		return dataSourceInfoList;
	}
	
	/**
	 * Returns given named data source info.
	 * @param name
	 * @return
	 */
	public DataSourceInfo getDataSourceInfo(String name) {
		for(DataSourceInfo dataSourceInfo : dataSourceInfoList) {
			if(dataSourceInfo.getDataSourceName().equals(name))
				return dataSourceInfo;
		}
		return null;
	}
	
	/**
	 * Adds given data source info to the driver info.
	 * @param dataSourceInfo
	 */
	public void addDataSourceInfo(DataSourceInfo dataSourceInfo) {
		if(dataSourceInfo == null)
			return;
		dataSourceInfoList.add(dataSourceInfo);
		Collections.sort(dataSourceInfoList);
		dataSourceInfo.setDriverInfo(this);
	}
	
	/**
	 * Removes given data source info from the driver info.
	 * @param dataSourceInfo
	 */
	public void removeDataSourceInfo(DataSourceInfo dataSourceInfo) {
		if(dataSourceInfo == null)
			return;
		if(dataSourceInfoList.remove(dataSourceInfo))
			dataSourceInfo.setDriverInfo(null);
		Collections.sort(dataSourceInfoList);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return driverName;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj == this)
			return true;
		if(!(obj instanceof DriverInfo))
			return false;
		DriverInfo other = (DriverInfo)obj;
		return new EqualsBuilder().append(this.driverClass, other.driverClass).toBoolean();
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(DriverInfo other) {
		if(other == null)
			return 1;
		if(this == other)
			return 0;
		return new CompareToBuilder().append(this.driverName, other.driverName).toInt();
	}
	
	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected Object clone() {
		return cloneObject();
	}

	/**
	 * @see com.vasnabilisim.util.Cloneable#cloneObject()
	 */
	public DriverInfo cloneObject() {
		return new DriverInfo(this);
	}
	
	/**
	 * Creates a sql driver from this.
	 * @return
	 * @throws JdbcException
	 */
	public Driver createDriver() throws SQLException {
		try {
			Class<? extends Driver> driverClass = com.vasnabilisim.util.ClassLoader.loadClass(this.driverClass, Driver.class);
			Driver driver = driverClass.newInstance();
			return driver;
		} catch (ClassNotFoundException | ClassCastException e) {
			String message = String.format("Unable to find driver %s.", this.driverClass);
			throw new SQLException(message, e);
		} catch (InstantiationException | IllegalAccessException e) {
			String message = String.format("Unable to create driver %s.", this.driverClass);
			throw new SQLException(message, e);
		}
	}
	
}
