package com.vasnabilisim.dataconverter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

/**
 * java.util.Date type converter. 
 * Converts to java.util.Date, java.sql.Timestamp, java.sql.Date, java.sql.Time, LocalDateTime, LocalDate, LocalTime, String
 *  
 * @author Menderes Fatih GUVEN
 */
public class DateConverter extends DataConverter<java.util.Date> {

	private static final long MILLIS_OF_DAY = TimeUnit.MILLISECONDS.convert(1L, TimeUnit.DAYS);
	
	
	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<java.util.Date> getFromClass() {
		return java.util.Date.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {java.util.Date.class, java.sql.Timestamp.class, java.sql.Date.class, java.sql.Time.class, LocalDateTime.class, LocalDate.class, LocalTime.class, String.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(java.util.Date value, Class<T> toClass) {
        if(value == null)
            return null;
        Object result = null;
        if(toClass.isAssignableFrom(java.util.Date.class)) 
        	result = value;
        if(toClass.isAssignableFrom(java.sql.Timestamp.class)) 
        	result = new java.sql.Timestamp(value.getTime());
        else if(toClass.isAssignableFrom(java.sql.Date.class)) 
        	result = new java.sql.Date(value.getTime() / MILLIS_OF_DAY * MILLIS_OF_DAY);
        else if(toClass.isAssignableFrom(java.sql.Time.class)) 
        	result = new java.sql.Time(value.getTime() % MILLIS_OF_DAY);
        else if(toClass.isAssignableFrom(LocalDateTime.class))
        	result = LocalDateTime.ofInstant(Instant.ofEpochMilli(value.getTime()), ZoneId.systemDefault());
        else if(toClass.isAssignableFrom(LocalDate.class))
        	result = LocalDateTime.ofInstant(Instant.ofEpochMilli(value.getTime()), ZoneId.systemDefault()).toLocalDate();
        else if(toClass.isAssignableFrom(LocalTime.class))
        	result = LocalDateTime.ofInstant(Instant.ofEpochMilli(value.getTime()), ZoneId.systemDefault()).toLocalTime();
        else if(toClass.isAssignableFrom(String.class)) 
        	result = value.toString();
        return toClass.cast(result);
	}

}
