package com.vasnabilisim.dataconverter;

import java.math.BigDecimal;

/**
 * Float type converter. 
 * Converts to Float, String, Integer, Long, Double, BigDecimal, Boolean. 
 * @author Menderes Fatih GUVEN
 */
public class FloatConverter extends DataConverter<Float> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<Float> getFromClass() {
		return Float.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {Float.class, String.class, Integer.class, Long.class, Double.class, BigDecimal.class, Boolean.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(Float value, Class<T> toClass) {
        if(value == null)
            return null;

        Object result = null;
        if(toClass.isAssignableFrom(Float.class))
        	result = value;
        else if(toClass.isAssignableFrom(String.class))
        	result = value.toString();  
        else if(toClass.isAssignableFrom(Integer.class))
        	result = value.intValue();
        else if(toClass.isAssignableFrom(Long.class))
        	result = value.longValue();
        else if(toClass.isAssignableFrom(Double.class))
        	result = value.doubleValue();
        else if(toClass.isAssignableFrom(BigDecimal.class))
        	result = new BigDecimal(value);
        else if(toClass.isAssignableFrom(Boolean.class))
        	result = value != 0.0f;
        return toClass.cast(result);
	}

}
