package com.vasnabilisim.dataconverter;

import java.math.BigDecimal;

/**
 * Boolean type converter. 
 * Converts to Boolean, String, Integer, Long, Float, Double, BigDecimal.
 * @author Menderes Fatih GUVEN
 */
public class BooleanConverter extends DataConverter<Boolean> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<Boolean> getFromClass() {
		return Boolean.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class<?>[] {Boolean.class, String.class, Integer.class, Long.class, Float.class, Double.class, BigDecimal.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(Boolean value, Class<T> toClass) {
        if(value == null)
            return null;
        Object result = null;
        if(toClass.isAssignableFrom(Boolean.class))
            result = value;
        else if(toClass.isAssignableFrom(String.class))
        	result = value.booleanValue() ? "1" : "0";
        else if(toClass.isAssignableFrom(Integer.class))
        	result = value.booleanValue() ? 1 : 0;
        else if(toClass.isAssignableFrom(Long.class))
        	result = value.booleanValue() ? 1l : 0l;
        else if(toClass.isAssignableFrom(Float.class))
        	result = value.booleanValue() ? 1.0f : 0.0f;
        else if(toClass.isAssignableFrom(Double.class))
        	result = value.booleanValue() ? 1.0 : 0.0;
        else if(toClass.isAssignableFrom(BigDecimal.class))
        	result = value.booleanValue() ? BigDecimal.ONE : BigDecimal.ZERO;
        return toClass.cast(result);
	}

}
