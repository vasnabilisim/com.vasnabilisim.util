package com.vasnabilisim.dataconverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * LocalDateTime type converter. 
 * Converts to LocalDateTime, LocalDate, LocalTime, String, java.sql.Timestamp, java.sql.Date, java.sql.Time, java.util.Date
 *  
 * @author Menderes Fatih GUVEN
 */
public class LocalDateTimeConverter extends DataConverter<LocalDateTime> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<LocalDateTime> getFromClass() {
		return LocalDateTime.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {LocalDateTime.class, LocalDate.class, LocalTime.class, String.class, java.sql.Timestamp.class, java.sql.Date.class, java.sql.Time.class, java.util.Date.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(LocalDateTime value, Class<T> toClass) {
        if(value == null)
            return null;
        Object result = null;
        if(toClass.isAssignableFrom(LocalDateTime.class))
        	result = value;
        else if(toClass.isAssignableFrom(LocalDate.class))
        	result = value.toLocalDate();
        else if(toClass.isAssignableFrom(LocalTime.class))
        	result = value.toLocalTime();
        else if(toClass.isAssignableFrom(String.class)) 
        	result = value.toString();
        else if(toClass.isAssignableFrom(java.sql.Timestamp.class)) 
        	result = java.sql.Timestamp.valueOf(value);
        else if(toClass.isAssignableFrom(java.sql.Date.class)) 
        	result = java.sql.Date.valueOf(value.toLocalDate());
        else if(toClass.isAssignableFrom(java.sql.Time.class)) 
        	result = java.sql.Time.valueOf(value.toLocalTime());
        else if(toClass.isAssignableFrom(java.util.Date.class)) 
        	result = java.util.Date.from(value.atZone(ZoneId.systemDefault()).toInstant());
        return toClass.cast(result);
	}

}
