package com.vasnabilisim.dataconverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

/**
 * java.sql.Timestamp type converter. 
 * Converts to java.sql.Timestamp, java.sql.Date, java.sql.Time, java.util.Date, LocalDateTime, LocalDate, LocalTime, String
 *  
 * @author Menderes Fatih GUVEN
 */
public class SqlTimestampConverter extends DataConverter<java.sql.Timestamp> {

	private static final long MILLIS_OF_DAY = TimeUnit.MILLISECONDS.convert(1L, TimeUnit.DAYS);
	
	
	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<java.sql.Timestamp> getFromClass() {
		return java.sql.Timestamp.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {java.sql.Timestamp.class, java.sql.Date.class, java.sql.Time.class, java.util.Date.class, LocalDateTime.class, LocalDate.class, LocalTime.class, String.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(java.sql.Timestamp value, Class<T> toClass) {
        if(value == null)
            return null;
        Object result = null;
        if(toClass.isAssignableFrom(java.sql.Timestamp.class)) 
        	result = value;
        else if(toClass.isAssignableFrom(java.sql.Date.class)) 
        	result = new java.sql.Date(value.getTime() / MILLIS_OF_DAY * MILLIS_OF_DAY);
        else if(toClass.isAssignableFrom(java.sql.Time.class)) 
        	result = new java.sql.Time(value.getTime() % MILLIS_OF_DAY);
        else if(toClass.isAssignableFrom(java.util.Date.class)) 
        	result = new java.util.Date(value.getTime());
        else if(toClass.isAssignableFrom(LocalDateTime.class))
        	result = value.toLocalDateTime();
        else if(toClass.isAssignableFrom(LocalDate.class))
        	result = value.toLocalDateTime().toLocalDate();
        else if(toClass.isAssignableFrom(LocalTime.class))
        	result = value.toLocalDateTime().toLocalTime();
        else if(toClass.isAssignableFrom(String.class)) 
        	result = value.toString();
        return toClass.cast(result);
	}

}
