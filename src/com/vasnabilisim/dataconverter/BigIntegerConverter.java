package com.vasnabilisim.dataconverter;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * BigDecimal type converter. 
 * Converts to BigInteger, BigDecimal, String, Integer, Long, Float, Double, Boolean. 
 * 
 * @author Menderes Fatih GUVEN
 */
public class BigIntegerConverter extends DataConverter<BigInteger> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<BigInteger> getFromClass() {
		return BigInteger.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {BigInteger.class, BigDecimal.class, String.class, Integer.class, Long.class, Float.class, Double.class, Boolean.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(BigInteger value, Class<T> toClass) {
        if(value == null)
            return null;

        Object result = null;
        if(toClass.isAssignableFrom(BigInteger.class))
        	result = value;
        else if(toClass.isAssignableFrom(BigDecimal.class))
        	result = new BigDecimal(value);
        else if(toClass.isAssignableFrom(String.class))
        	result = value.toString();  
        else if(toClass.isAssignableFrom(Integer.class))
        	result = value.intValue();
        else if(toClass.isAssignableFrom(Long.class))
        	result = value.longValue();
        else if(toClass.isAssignableFrom(Float.class))
        	result = value.floatValue();
        else if(toClass.isAssignableFrom(Double.class))
        	result = value.doubleValue();
        else if(toClass.isAssignableFrom(Boolean.class))
        	result = value.intValue() != 0;
        return toClass.cast(result);
	}

}
