package com.vasnabilisim.dataconverter;

import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * String type converter. 
 * Converts to String, Integer, Long, Float, BigInteger, BigDecimal, Double, Boolean. 
 * 
 * @author Menderes Fatih GUVEN
 */
public class CharacterConverter extends DataConverter<Character> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<Character> getFromClass() {
		return Character.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {String.class, Integer.class, Long.class, Float.class, Double.class, BigInteger.class, BigDecimal.class, Boolean.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(Character value, Class<T> toClass) {
        if(value == null)
            return null;

        Object result = null;
        if(toClass.isAssignableFrom(String.class))
        	result = value.toString();
        else if(toClass.isAssignableFrom(Integer.class)) 
        	result = value.charValue() - '0';
        else if(toClass.isAssignableFrom(Long.class)) 
        	result = (long)(value.charValue() - '0');
        else if(toClass.isAssignableFrom(Float.class)) 
        	result = (float)(value.charValue() - '0');
        else if(toClass.isAssignableFrom(Double.class)) 
        	result = (double)(value.charValue() - '0');
        else if(toClass.isAssignableFrom(BigInteger.class)) 
        	result = BigInteger.valueOf(value.charValue() - '0');
        else if(toClass.isAssignableFrom(BigInteger.class)) 
        	result = new BigDecimal(value.charValue() - '0');
        else if(toClass.isAssignableFrom(Boolean.class)) 
        	result = !(value.charValue() == 0 || value.charValue() == '0');
        return toClass.cast(result);
	}

}
