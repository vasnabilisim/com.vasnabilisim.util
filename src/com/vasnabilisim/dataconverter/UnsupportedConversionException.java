package com.vasnabilisim.dataconverter;

import java.io.Serializable;

/**
 * UnsupportedConversionException
 * 
 * @author Menderes Fatih GUVEN
 */
public class UnsupportedConversionException extends RuntimeException implements
		Serializable {
	private static final long serialVersionUID = -2602566633588206950L;

	public UnsupportedConversionException() {
	}

	public UnsupportedConversionException(String message) {
		super(message);
	}

	public UnsupportedConversionException(Throwable cause) {
		super(cause);
	}

	public UnsupportedConversionException(String message, Throwable cause) {
		super(message, cause);
	}
}
