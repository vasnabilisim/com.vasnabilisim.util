package com.vasnabilisim.dataconverter;

import java.util.UUID;

/**
 * UUID type converter. 
 * Converts to UUID and String. 
 * @author Menderes Fatih GUVEN
 */
public class UUIDConverter extends DataConverter<UUID> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<UUID> getFromClass() {
		return UUID.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {UUID.class, String.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(UUID value, Class<T> toClass) {
        if(value == null)
            return null;

        Object result = null;
        if(toClass.isAssignableFrom(UUID.class))
        	result = value;  
        else if(toClass.isAssignableFrom(String.class))
        	result = value.toString();  
        return toClass.cast(result);
	}
}
