package com.vasnabilisim.dataconverter;

/**
 * java.util.Date type converter. 
 * Converts to java.util.Date, java.sql.Timestamp, java.sql.Date, java.sql.Time, LocalDateTime, LocalDate, LocalTime, String
 *  
 * @author Menderes Fatih GUVEN
 */
public class InstantConverter extends DataConverter<java.time.Instant> {

	private static final long MILLIS_OF_DAY = java.util.concurrent.TimeUnit.MILLISECONDS.convert(1L, java.util.concurrent.TimeUnit.DAYS);
	
	
	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<java.time.Instant> getFromClass() {
		return java.time.Instant.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {
				java.time.Instant.class, 
				java.util.Date.class, java.sql.Timestamp.class, java.sql.Date.class, java.sql.Time.class, 
				java.time.LocalDateTime.class, java.time.LocalDate.class, java.time.LocalTime.class, 
				String.class
			};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(java.time.Instant value, Class<T> toClass) {
        if(value == null)
            return null;
        Object result = null;
        if(toClass.isAssignableFrom(java.time.Instant.class)) 
        	result = value;
        if(toClass.isAssignableFrom(java.sql.Timestamp.class)) 
        	result = new java.sql.Timestamp(value.toEpochMilli());
        else if(toClass.isAssignableFrom(java.sql.Date.class)) 
        	result = new java.sql.Date(value.toEpochMilli() / MILLIS_OF_DAY * MILLIS_OF_DAY);
        else if(toClass.isAssignableFrom(java.sql.Time.class)) 
        	result = new java.sql.Time(value.toEpochMilli() % MILLIS_OF_DAY);
        else if(toClass.isAssignableFrom(java.util.Date.class)) 
        	result = new java.util.Date(value.toEpochMilli());
        else if(toClass.isAssignableFrom(java.time.LocalDateTime.class))
        	result = java.time.LocalDateTime.ofInstant(value, java.time.ZoneId.systemDefault());
        else if(toClass.isAssignableFrom(java.time.LocalDate.class))
        	result = java.time.LocalDateTime.ofInstant(value, java.time.ZoneId.systemDefault()).toLocalDate();
        else if(toClass.isAssignableFrom(java.time.LocalTime.class))
        	result = java.time.LocalDateTime.ofInstant(value, java.time.ZoneId.systemDefault()).toLocalTime();
        else if(toClass.isAssignableFrom(String.class)) 
        	result = value.toString();
        return toClass.cast(result);
	}

}
