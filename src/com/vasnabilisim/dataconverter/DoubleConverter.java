package com.vasnabilisim.dataconverter;

import java.math.BigDecimal;

/**
 * Double type converter. 
 * Converts to Double, String, Integer, Long, Float, BigDecimal, Boolean. 
 * @author Menderes Fatih GUVEN
 */
public class DoubleConverter extends DataConverter<Double> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<Double> getFromClass() {
		return Double.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {Double.class, String.class, Integer.class, Long.class, Float.class, BigDecimal.class, Boolean.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(Double value, Class<T> toClass) {
        if(value == null)
            return null;

        Object result = null;
        if(toClass.isAssignableFrom(Double.class))
        	result = value;
        else if(toClass.isAssignableFrom(String.class))
        	result = value.toString();  
        else if(toClass.isAssignableFrom(Integer.class))
        	result = value.intValue();
        else if(toClass.isAssignableFrom(Long.class))
        	result = value.longValue();
        else if(toClass.isAssignableFrom(Float.class))
        	result = value.floatValue();
        else if(toClass.isAssignableFrom(BigDecimal.class))
        	result = new BigDecimal(value);
        else if(toClass.isAssignableFrom(Boolean.class))
        	result = value != 0.0;
        return toClass.cast(result);
	}

}
