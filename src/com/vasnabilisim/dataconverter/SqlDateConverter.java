package com.vasnabilisim.dataconverter;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * java.sql.Date type converter. 
 * Converts to java.sql.Date, java.sql.Timestamp, java.util.Date, LocalDateTime, LocalDate, String
 *  
 * @author Menderes Fatih GUVEN
 */
public class SqlDateConverter extends DataConverter<java.sql.Date> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<java.sql.Date> getFromClass() {
		return java.sql.Date.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {java.sql.Date.class, java.sql.Timestamp.class, java.util.Date.class, LocalDateTime.class, LocalDate.class, String.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(java.sql.Date value, Class<T> toClass) {
        if(value == null)
            return null;
        Object result = null;
        if(toClass.isAssignableFrom(java.sql.Date.class)) 
        	result = value;
        else if(toClass.isAssignableFrom(java.sql.Timestamp.class)) 
        	result = new java.sql.Timestamp(value.getTime());
        else if(toClass.isAssignableFrom(java.util.Date.class)) 
        	result = new java.util.Date(value.getTime());
        else if(toClass.isAssignableFrom(LocalDateTime.class))
        	result = value.toLocalDate().atStartOfDay();
        else if(toClass.isAssignableFrom(LocalDate.class))
        	result = value.toLocalDate();
        else if(toClass.isAssignableFrom(String.class)) 
        	result = value.toString();
        return toClass.cast(result);
	}

}
