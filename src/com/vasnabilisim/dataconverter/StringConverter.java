package com.vasnabilisim.dataconverter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

/**
 * String type converter. 
 * Converts to String, Integer, Long, Float, Double, BigInteger, BigDecimal, Boolean, UUID, LocalDateTime, LocalDate, LocalTime. 
 * 
 * @author Menderes Fatih GUVEN
 */
public class StringConverter extends DataConverter<String> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<String> getFromClass() {
		return String.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {String.class, Integer.class, Long.class, Float.class, Double.class, BigInteger.class, BigDecimal.class, Boolean.class, UUID.class, LocalDateTime.class, LocalDate.class, LocalTime.class};
        //TODO java.sql.Timestamp, java.sql.Date, java.sql.Time, java.util.Date 
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(String value, Class<T> toClass) {
        if(value == null)
            return null;

        Object result = null;
        if(toClass.isAssignableFrom(String.class))
        	result = value;
        else if(toClass.isAssignableFrom(Integer.class)) 
        	result = Integer.parseInt(value);
        else if(toClass.isAssignableFrom(Long.class)) 
        	result = Long.parseLong(value);
        else if(toClass.isAssignableFrom(Float.class)) 
        	result = Float.parseFloat(value);
        else if(toClass.isAssignableFrom(Double.class)) 
        	result = Double.parseDouble(value);
        else if(toClass.isAssignableFrom(BigInteger.class)) 
        	result = new BigInteger(value);
        else if(toClass.isAssignableFrom(BigDecimal.class)) 
        	result = new BigDecimal(value);
        else if(toClass.isAssignableFrom(Boolean.class)) 
        	result = "1".equals(value) || "true".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value);
        else if(toClass.isAssignableFrom(UUID.class)) 
        	result = UUID.fromString(value);
        else if(toClass.isAssignableFrom(LocalDateTime.class)) 
        	result = LocalDateTime.parse(value);
        else if(toClass.isAssignableFrom(LocalDate.class)) 
        	result = LocalDate.parse(value);
        else if(toClass.isAssignableFrom(LocalTime.class)) 
        	result = LocalTime.parse(value);
        return toClass.cast(result);
	}

}
