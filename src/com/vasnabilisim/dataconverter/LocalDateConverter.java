package com.vasnabilisim.dataconverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * LocalDate type converter. 
 * Converts to LocalDate, LocalDateTime, String, java.sql.Date, java.sql.Timestamp, java.util.Date
 *  
 * @author Menderes Fatih GUVEN
 */
public class LocalDateConverter extends DataConverter<LocalDate> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<LocalDate> getFromClass() {
		return LocalDate.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {LocalDate.class, LocalDateTime.class, String.class, java.sql.Date.class, java.sql.Timestamp.class, java.util.Date.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(LocalDate value, Class<T> toClass) {
        if(value == null)
            return null;
        Object result = null;
        if(toClass.isAssignableFrom(LocalDate.class))
        	result = value;
        else if(toClass.isAssignableFrom(LocalDateTime.class))
        	result = value.atStartOfDay();
        else if(toClass.isAssignableFrom(String.class)) 
        	result = value.toString();
        else if(toClass.isAssignableFrom(java.sql.Date.class)) 
        	result = java.sql.Date.valueOf(value);
        else if(toClass.isAssignableFrom(java.sql.Timestamp.class)) 
        	result = java.sql.Timestamp.valueOf(value.atStartOfDay());
        else if(toClass.isAssignableFrom(java.util.Date.class)) 
        	result = java.util.Date.from(value.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        return toClass.cast(result);
	}

}
