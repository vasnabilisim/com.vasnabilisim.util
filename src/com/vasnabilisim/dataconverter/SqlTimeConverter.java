package com.vasnabilisim.dataconverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * java.sql.Time type converter. 
 * Converts to java.sql.Time, java.sql.Timestamp, java.util.Date, LocalDateTime, LocalTime, String
 *  
 * @author Menderes Fatih GUVEN
 */
public class SqlTimeConverter extends DataConverter<java.sql.Time> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<java.sql.Time> getFromClass() {
		return java.sql.Time.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {java.sql.Time.class, java.sql.Timestamp.class, java.util.Date.class, LocalDateTime.class, LocalTime.class, String.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(java.sql.Time value, Class<T> toClass) {
        if(value == null)
            return null;
        Object result = null;
        if(toClass.isAssignableFrom(java.sql.Time.class)) 
        	result = value;
        else if(toClass.isAssignableFrom(java.sql.Timestamp.class)) 
        	result = new java.sql.Timestamp(value.getTime());
        else if(toClass.isAssignableFrom(java.util.Date.class)) 
        	result = new java.util.Date(value.getTime());
        else if(toClass.isAssignableFrom(LocalDateTime.class))
        	result = LocalDateTime.of(LocalDate.of(0, 1, 1), value.toLocalTime());
        else if(toClass.isAssignableFrom(LocalTime.class))
        	result = value.toLocalTime();
        else if(toClass.isAssignableFrom(String.class)) 
        	result = value.toString();
        return toClass.cast(result);
	}

}
