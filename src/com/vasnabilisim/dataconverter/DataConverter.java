package com.vasnabilisim.dataconverter;

import java.util.Map;
import java.util.TreeMap;

import com.vasnabilisim.util.ClassComparator;

/**
 * Abstract base class of converters.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class DataConverter<F> {
	/**
	 * List of registered converters.
	 */
	private static Map<Class<?>, DataConverter<?>> converterMap = new TreeMap<Class<?>, DataConverter<?>>(new ClassComparator());

	static {
		DataConverter.register(new BooleanConverter());
		DataConverter.register(new CharacterConverter());
		DataConverter.register(new LocalDateTimeConverter());
		DataConverter.register(new LocalDateConverter());
		DataConverter.register(new LocalTimeConverter());
		DataConverter.register(new SqlTimestampConverter());
		DataConverter.register(new SqlDateConverter());
		DataConverter.register(new SqlTimeConverter());
		DataConverter.register(new DateConverter());
		DataConverter.register(new BigDecimalConverter());
		DataConverter.register(new DoubleConverter());
		DataConverter.register(new FloatConverter());
		DataConverter.register(new BigIntegerConverter());
		DataConverter.register(new LongConverter());
		DataConverter.register(new IntegerConverter());
		DataConverter.register(new StringConverter());
		DataConverter.register(new UUIDConverter());
	}

	/**
	 * Registers the given converter to be used in conversion operators.
	 * 
	 * @param converter
	 */
	public static <F> void register(DataConverter<F> converter) {
		converterMap.put(converter.getFromClass(), converter);
	}

	/**
	 * Returns the converter which is capable to convert from first given type
	 * to second given type.
	 * 
	 * @param fromClass
	 * @param toClass
	 * @param useCompound
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <F, T> DataConverter<F> getConverter(Class<F> fromClass, Class<T> toClass, boolean useCompound) {
		DataConverter<?> converter = converterMap.get(fromClass);
		if (converter != null) {
			if (converter.canConvertTo(toClass))
				return (DataConverter<F>) converter;
		}
		for (Map.Entry<Class<?>, DataConverter<?>> entry : converterMap.entrySet()) {
			if (entry.getKey().isAssignableFrom(fromClass)) {
				converter = entry.getValue();
				if (converter.canConvertTo(toClass))
					return (DataConverter<F>) converter;
			}
		}
		if (!useCompound)
			return null;
		DataConverter<String> secondConverter = getConverter(String.class, toClass, false);
		if(secondConverter == null)
			return null;
		DataConverter<F> firstConverter = getConverter(fromClass, String.class, false);
		if(firstConverter == null)
			return null;
		return new CompoundConverter<F>(firstConverter, secondConverter);
	}

	/**
	 * Returns the converter which is capable to convert given type to other
	 * types.
	 * 
	 * @param fromClass
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <F> DataConverter<F> getConverter(Class<F> fromClass) {
		DataConverter<?> converter = converterMap.get(fromClass);
		if (converter != null)
			return (DataConverter<F>) converter;
		for (Map.Entry<Class<?>, DataConverter<?>> entry : converterMap
				.entrySet()) {
			if (entry.getKey().isAssignableFrom(fromClass))
				return (DataConverter<F>) entry.getValue();
		}
		return null;
	}

	/**
	 * Converts the given source to given type. Looks for the converter which is
	 * capable to make the conversions. If no converters found, returns null.
	 * 
	 * @param <F>
	 * @param <T>
	 * @param source
	 * @param toClass
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <F, T> T convertTo(F source, Class<T> toClass) {
		if (source == null)
			return null;
		return convertTo((Class<F>)source.getClass(), source, toClass);
	}

	/**
	 * Converts the given source to given type. Looks for the converter which is
	 * capable to make the conversions. If no converters found, returns null.
	 * 
	 * @param <F>
	 * @param <T>
	 * @param source
	 * @param toClass
	 * @return
	 */
	public static <F, T> T convertTo(Class<F> fromClass, Object source, Class<T> toClass) {
		if (source == null)
			return null;
		if(!fromClass.isInstance(source))
			throw new UnsupportedConversionException(String.format("Given object is of type %s. Expecting %s.", source.getClass().getName(), fromClass.getName()));
		if(fromClass.equals(toClass))
			return toClass.cast(source);
		DataConverter<F> converter = (DataConverter<F>) getConverter(fromClass, toClass, true);
		if (converter == null)
			throw new UnsupportedConversionException(String.format("Unable to convert %s to %s.", source.toString(), toClass.getName()));
		return converter.convert(fromClass.cast(source), toClass);
	}

	/**
	 * Returns the type of which this converter is capable to convert to other
	 * types.
	 * 
	 * @return
	 */
	public abstract Class<F> getFromClass();

	/**
	 * Returns the types of which this converter is capable to convert to.
	 * 
	 * @return
	 */
	public abstract Class<?>[] getToClasses();

	/**
	 * Converts the given source to given type.
	 * 
	 * @param source
	 * @param toClass
	 * @return
	 */
	public abstract <T> T convert(F source, Class<T> toClass);

	/**
	 * Converts the given source to given type.
	 * 
	 * @param source
	 * @param toClass
	 * @return
	 */
	public <T> T convertObject(Object source, Class<T> toClass) {
		return convert(getFromClass().cast(source), toClass);
	}

	/**
	 * Can this converter convert to given type.
	 * 
	 * @param toClass
	 * @return
	 */
	public boolean canConvertTo(Class<?> toClass) {
		Class<?>[] toClasses = getToClasses();
		for (Class<?> toClassIte : toClasses) {
			if (toClass.isAssignableFrom(toClassIte))
				return true;
		}
		return false;
	}

}