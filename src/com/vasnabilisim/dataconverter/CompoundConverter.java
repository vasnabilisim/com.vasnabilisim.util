package com.vasnabilisim.dataconverter;

/**
 * Compound data converter.
 * @author Menderes Fatih GUVEN
 */
public class CompoundConverter<F> extends DataConverter<F> {

	//TODO re implement using classes other that string.
	
	DataConverter<F> firstDataConverter;
	DataConverter<String> secondDataConverter;
	
	public CompoundConverter(DataConverter<F> firstDataConverter, DataConverter<String> secondDataConverter) {
		this.firstDataConverter = firstDataConverter;
		this.secondDataConverter = secondDataConverter;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<F> getFromClass() {
		return firstDataConverter.getFromClass();
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return secondDataConverter.getToClasses();
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(F source, Class<T> toClass) {
		return secondDataConverter.convert(firstDataConverter.convert(source, String.class), toClass);
	}
}
