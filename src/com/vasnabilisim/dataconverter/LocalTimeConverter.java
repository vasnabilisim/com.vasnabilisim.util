package com.vasnabilisim.dataconverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * LocalDate type converter. 
 * Converts to LocalTime, LocalDateTime, String, java.sql.Time, java.sql.Timestamp, java.util.Date
 *  
 * @author Menderes Fatih GUVEN
 */
public class LocalTimeConverter extends DataConverter<LocalTime> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<LocalTime> getFromClass() {
		return LocalTime.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {LocalTime.class, LocalDateTime.class, String.class, java.sql.Time.class, java.sql.Timestamp.class, java.util.Date.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(LocalTime value, Class<T> toClass) {
        if(value == null)
            return null;
        Object result = null;
        if(toClass.isAssignableFrom(LocalTime.class))
        	result = value;
        else if(toClass.isAssignableFrom(LocalDateTime.class))
        	result = LocalDateTime.of(LocalDate.of(0, 1, 1), value);
        else if(toClass.isAssignableFrom(String.class)) 
        	result = value.toString();
        else if(toClass.isAssignableFrom(java.sql.Timestamp.class)) 
        	result = java.sql.Timestamp.valueOf(value.atDate(LocalDate.of(0, 1, 1)));
        else if(toClass.isAssignableFrom(java.sql.Time.class)) 
        	result = java.sql.Time.valueOf(value);
        else if(toClass.isAssignableFrom(java.util.Date.class)) 
        	result = java.util.Date.from(value.atDate(LocalDate.of(0, 1, 1)).atZone(ZoneId.systemDefault()).toInstant());
        return toClass.cast(result);
	}

}
