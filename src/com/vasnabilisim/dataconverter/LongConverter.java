package com.vasnabilisim.dataconverter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;

/**
 * Long type converter. 
 * Converts to Long, Date, Calendar, String, Integer, Float, Double, BigInteger, BigDecimal, Boolean. 
 * @author Menderes Fatih GUVEN
 */
public class LongConverter extends DataConverter<Long> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<Long> getFromClass() {
		return Long.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {Long.class, Date.class, Calendar.class, String.class, Integer.class, Float.class, Double.class, BigInteger.class, BigDecimal.class, Boolean.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(Long value, Class<T> toClass) {
        if(value == null)
            return null;

        Object result = null;
        if(toClass.isAssignableFrom(Long.class))
        	result = value;
        else if(toClass.isAssignableFrom(Date.class))
        	result = new Date(value.longValue());
        else if(toClass.isAssignableFrom(Calendar.class)) 
        {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(value.longValue());
            result = cal;
        }
        else if(toClass.isAssignableFrom(String.class))
        	result = value.toString();  
        else if(toClass.isAssignableFrom(Integer.class))
        	result = value.intValue();
        else if(toClass.isAssignableFrom(Float.class))
        	result = value.floatValue();
        else if(toClass.isAssignableFrom(Double.class))
        	result = value.doubleValue();
        else if(toClass.isAssignableFrom(BigInteger.class))
        	result = BigInteger.valueOf(value);
        else if(toClass.isAssignableFrom(BigDecimal.class))
        	result = new BigDecimal(value);
        else if(toClass.isAssignableFrom(Boolean.class))
        	result = value != 0l;
        return toClass.cast(result);
	}

}
