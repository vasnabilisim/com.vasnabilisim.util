package com.vasnabilisim.dataconverter;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Integer type converter. 
 * Converts to Integer, String, Long, Float, Double, BigInteger, BigDecimal, Boolean. 
 * @author Menderes Fatih GUVEN
 */
public class IntegerConverter extends DataConverter<Integer> {

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getFromClass()
	 */
	@Override
	public Class<Integer> getFromClass() {
		return Integer.class;
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#getToClasses()
	 */
	@Override
	public Class<?>[] getToClasses() {
		return new Class[] {Integer.class, String.class, Long.class, Float.class, Double.class, BigInteger.class, BigDecimal.class, Boolean.class};
	}

	/**
	 * @see com.vasnabilisim.dataconverter.DataConverter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(Integer value, Class<T> toClass) {
        if(value == null)
            return null;

        Object result = null;
        if(toClass.isAssignableFrom(Integer.class))
        	result = value;
        else if(toClass.isAssignableFrom(String.class))
        	result = value.toString();  
        else if(toClass.isAssignableFrom(Long.class))
        	result = value.longValue();
        else if(toClass.isAssignableFrom(Float.class))
        	result = value.floatValue();
        else if(toClass.isAssignableFrom(Double.class))
        	result = value.doubleValue();
        else if(toClass.isAssignableFrom(BigInteger.class))
        	result = BigInteger.valueOf(value);
        else if(toClass.isAssignableFrom(BigDecimal.class))
        	result = new BigDecimal(value);
        else if(toClass.isAssignableFrom(Boolean.class))
        	result = value != 0;
        return toClass.cast(result);
	}

}
