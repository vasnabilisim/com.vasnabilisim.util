package com.vasnabilisim.text;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.MinimalHTMLWriter;
import javax.swing.text.rtf.RTFEditorKit;

/**
 * @author Menderes Fatih GUVEN
 */
public final class TextUtil {

	private TextUtil() {
	}
	
	static {
		initTranslationTable();
	}

	public static char[] rtfTranslationTable = null;
	
	static void initTranslationTable() {
		try {
			Class<?> clazz = Class.forName("javax.swing.text.rtf.RTFReader");
			Method method = clazz.getMethod("getCharacterSet", String.class);
			method.setAccessible(true);
			rtfTranslationTable = (char[]) method.invoke(null, "ansi");
			rtfTranslationTable[221] = '�';
			rtfTranslationTable[222] = '�';
			rtfTranslationTable[170] = '�';
			rtfTranslationTable[186] = '�';
			//add further translations
		} catch (ClassNotFoundException | NoSuchMethodException
				| SecurityException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) 
		{
		}
	}
	
	public void setRtfTranslation(int intValue, char charValue) {
		rtfTranslationTable[intValue] = charValue;
	}

	public static String fromRtfToText(String rtf) {
		return rtf == null ? null : fromRtfToText(new StringReader(rtf));
	}

	public static String fromRtfToText(InputStream in) {
		try {
			return fromRtfToText(new InputStreamReader(in, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static String fromRtfToText(Reader in) {
		RTFEditorKit rtfParser = new RTFEditorKit();
		Document document = rtfParser.createDefaultDocument();
		try {
			rtfParser.read(in, document, 0);
			return document.getText(0, document.getLength());
		} catch (IOException | BadLocationException e) {
			return null;
		}
	}

	public static void fromRtfToText(String rtf, Writer out) {
		fromRtfToText(new StringReader(rtf), out);
	}

	public static void fromRtfToText(InputStream in, Writer out) {
		try {
			fromRtfToText(new InputStreamReader(in, "UTF-8"), out);
		} catch (UnsupportedEncodingException e) {
		}
	}

	public static void fromRtfToText(Reader in, Writer out) {
		RTFEditorKit rtfParser = new RTFEditorKit();
		Document document = rtfParser.createDefaultDocument();
		try {
			rtfParser.read(in, document, 0);
			out.write(document.getText(0, document.getLength()));
		} catch (IOException | BadLocationException e) {
		}
	}

	public static String fromRtfToHtml(String rtf) {
		return rtf == null ? null : fromRtfToHtml(new StringReader(rtf));
	}

	public static String fromRtfToHtml(InputStream in) {
		try {
			return fromRtfToHtml(new InputStreamReader(in, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static String fromRtfToHtml(Reader in) {
		try {
			RTFEditorKit rtfParser = new RTFEditorKit();
			Document document = rtfParser.createDefaultDocument();
			rtfParser.read(in, document, 0);
			StringWriter out = new StringWriter();
			CustomHTTPWriter httpWriter = new CustomHTTPWriter(out, (StyledDocument) document);
			httpWriter.write();
			return out.toString();
		} catch (IOException | BadLocationException e) {
			return null;
		}
	}

	public static void fromRtfToHtml(String rtf, Writer out) {
		fromRtfToHtml(new StringReader(rtf), out);
	}

	public static void fromRtfToHtml(String rtf, OutputStream out) {
		try {
			fromRtfToHtml(new StringReader(rtf), new OutputStreamWriter(out, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
		}
	}

	public static void fromRtfToHtml(InputStream in, OutputStream out) {
		try {
			fromRtfToHtml(new InputStreamReader(in, "UTF-8"), new OutputStreamWriter(out, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
		}
	}

	public static void fromRtfToHtml(InputStream in, Writer out) {
		try {
			fromRtfToHtml(new InputStreamReader(in, "UTF-8"), out);
		} catch (UnsupportedEncodingException e) {
		}
	}

	public static void fromRtfToHtml(Reader in, OutputStream out) {
		try {
			fromRtfToHtml(in, new OutputStreamWriter(out, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
		}
	}

	public static void fromRtfToHtml(Reader in, Writer out) {
		try {
			RTFEditorKit rtfParser = new RTFEditorKit();
			Document document = rtfParser.createDefaultDocument();
			rtfParser.read(in, document, 0);
			CustomHTTPWriter httpWriter = new CustomHTTPWriter(out, (StyledDocument) document);
			httpWriter.write();
			out.flush();
		} catch (IOException | BadLocationException e) {
		}
	}
	
	static class CustomHTTPWriter extends MinimalHTMLWriter {
		public CustomHTTPWriter(Writer w, StyledDocument doc) {
			super(w, doc);
		}
		protected void writeHeader() throws IOException {
	        writeStartTag("<head>");
	        writeStartTag("<meta charset=\"utf-8\">");
	        writeEndTag("</meta>");
	        writeStartTag("<style>");
	        writeStartTag("<!--");
	        writeStyles();
	        writeEndTag("-->");
	        writeEndTag("</style>");
	        writeEndTag("</head>");
		}
	}
}
