package com.vasnabilisim.io;

/**
 * @author Menderes Fatih GUVEN
 */
public class JarFileFilter extends FileFilter {

	/**
	 * Default constructor.
	 */
	public JarFileFilter() {
		super("jar", "Java Archive");
	}

}
