package com.vasnabilisim.io;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.StringTokenizer;

/**
 * @author Menderes Fatih GUVEN
 */
public class CsvReader implements Closeable {

	public static int MAX_COLUMNS = 20;
	
	int columnCount;
	String[] cellBuffer;

	char seperator = ',';
	BufferedReader reader;
	int currentLineNumber = 0;

	public CsvReader(File in) throws FileNotFoundException {
		this(in, MAX_COLUMNS);
	}
	
	public CsvReader(File in, int columnCount) throws FileNotFoundException {
		this(new FileReader(in), columnCount);
	}
	
	public CsvReader(InputStream in) {
		this(in, MAX_COLUMNS);
	}
	
	public CsvReader(InputStream in, int columnCount) {
		this(new InputStreamReader(in), columnCount);
	}
	
	public CsvReader(Reader in) {
		this(in, MAX_COLUMNS);
	}
	
	public CsvReader(Reader in, int columnCount) {
		this(new BufferedReader(in), columnCount);
	}
	
	public CsvReader(BufferedReader in) {
		this(in, MAX_COLUMNS);
	}
	
	public CsvReader(BufferedReader in, int columnCount) {
		reader = in;
		this.columnCount = columnCount;
		this.cellBuffer = new String[columnCount];
	}
	
	public char getSeperator() {
		return seperator;
	}
	
	public void setSeperator(char seperator) {
		this.seperator = seperator;
	}

	public void close() throws IOException {
		reader.close();
	}
	
	public String[] readLine() throws IOException {
		String currentLine = reader.readLine();
		++currentLineNumber;
		if(currentLine == null)
			return null;
		StringTokenizer tokenizer = new StringTokenizer(currentLine, String.valueOf(seperator), true);
		int index = 0;
		boolean expectingSeperator = false;
		while(tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if(expectingSeperator) {
				if(token.length() != 1 || token.charAt(0) != seperator) {
					throw new IllegalStateException("Expecting seperator, found[" + token + "]");
				}
				expectingSeperator = false;
			}
			else {
				if(token.length() == 1 && token.charAt(0) == seperator) {
					cellBuffer[index++] = null;
					expectingSeperator = false;
				}
				else {
					cellBuffer[index++] = token;
					expectingSeperator = true;
				}
			}
		}
		if(!expectingSeperator)
			cellBuffer[index++] = null;
		String[] cells = new String[index];
		System.arraycopy(cellBuffer, 0, cells, 0, index);
		return cells;
	}
	
	public int getCurrentLineNumber() {
		return currentLineNumber;
	}
}
