package com.vasnabilisim.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.StringTokenizer;

import com.vasnabilisim.util.StringUtil;

/**
 * @author Menderes Fatih GUVEN
 */
public class CsvWriter {

	char seperator = ',';
	BufferedWriter writer;
	int currentLineNumber = 0;
	int currentColumnNumber = 0;

	public CsvWriter(File out) throws IOException {
		this(new FileWriter(out));
	}
	
	public CsvWriter(OutputStream out) {
		this(new OutputStreamWriter(out));
	}
	
	public CsvWriter(Writer out) {
		this(new BufferedWriter(out));
	}

	public CsvWriter(BufferedWriter out) {
		writer = out;
	}

	public char getSeperator() {
		return seperator;
	}
	
	public void setSeperator(char seperator) {
		this.seperator = seperator;
	}

	public void close() throws IOException {
		writer.close();
	}
	
	public void writeLine(String[] texts) throws IOException {
		for(String text : texts)
			write(text);
		nextLine();
	}
	
	public void write(String text) throws IOException {
		if(currentColumnNumber > 0)
			writer.write(seperator);
		if(text != null)
			writer.write(encode(text));
		++currentColumnNumber;
	}

	public void nextLine() throws IOException {
		writer.write(System.lineSeparator());
		++currentLineNumber;
		currentColumnNumber = 0;
	}
	
	public void writeLine(Object[] values) throws IOException {
		writeLine(StringUtil.toString(values));
	}
	
	protected String encode(String text) {
		if(text == null)
			return null;
		StringTokenizer tokenizer = new StringTokenizer(text, String.valueOf(seperator), true);
		int count = tokenizer.countTokens();
		if(count == 1)
			return text;
		StringBuilder builder = new StringBuilder(text.length() + count);
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if(token.length() == 1 && seperator == token.charAt(0))
				builder.append((char)130);
			builder.append(token);
		}
		return builder.toString();
	}
	
	public int getCurrentLineNumber() {
		return currentLineNumber;
	}

	public void flush() throws IOException {
		writer.flush();
	}
}
