package com.vasnabilisim.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.vasnabilisim.util.Pair;

/**
 * Some useful io methods.
 * @author Menderes Fatih GUVEN
 */
public final class IOUtil {

	private IOUtil() {}
	
	public static int BUFFER_SIZE = 4096;
	

	public static void transfer(byte[] byteArray, String filePath) throws IOException {
		FileOutputStream out = new FileOutputStream(filePath);
		try {
			transfer(byteArray, out);
		} finally {
			out.close();
		}
	}

	public static void transfer(byte[] byteArray, File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		try {
			transfer(byteArray, out);
		} finally {
			out.close();
		}
	}

	public static void transfer(byte[] byteArray, OutputStream out) throws IOException {
		out.write(byteArray);
	}

	public static void transfer(byte[] byteArray, int off, int len, OutputStream out) throws IOException {
		out.write(byteArray, off, len);
	}

	public static void transfer(String filePath, OutputStream out) throws IOException {
		FileInputStream in = new FileInputStream(filePath);
		try {
			transfer(in, out);
		} finally {
			in.close();
		}
	}
	
	public static void transfer(InputStream in, String filePath) throws IOException {
		FileOutputStream out = new FileOutputStream(filePath);
		try {
			transfer(in, out);
		} finally {
			out.close();
		}
	}
	
	public static void transfer(File file, OutputStream out) throws IOException {
		FileInputStream in = new FileInputStream(file);
		try {
			transfer(in, out);
		} finally {
			in.close();
		}
	}
	
	public static void transfer(InputStream in, File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		try {
			transfer(in, out);
		} finally {
			out.close();
		}
	}

	public static void transfer(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[BUFFER_SIZE];
		int length = -1;
		//long sum = 0l;
		while( (length = in.read(buffer)) >=0 ) {
			//sum += length;
			//try {
				out.write(buffer, 0, length);
			//} catch (IOException e) {
			//	System.out.println("SUM: " + sum);
			//	e.printStackTrace();
			//	throw e;
			//}
		}
	}
	
	public static Pair<String, String> findFileNameAndExtension(File file) {
		return (file == null || file.isDirectory()) ? null : findFileNameAndExtension(file.getName());
	}
	
	public static Pair<String, String> findFileNameAndExtension(String fileName) {
		Pair<String, String> pair = new Pair<>();
		if(fileName == null || fileName.length() == 0)
			return pair;
		int indexDot = fileName.lastIndexOf('.');
		if(indexDot < 0) {
			pair.setFirst(fileName);
		}
		else if(indexDot >= fileName.length()-1) {
			pair.setFirst(fileName.substring(0, indexDot));
		}
		else {
			pair.setFirst(fileName.substring(0, indexDot));
			pair.setSecond(fileName.substring(indexDot + 1));
		}
		return pair;
	}
	
	public static String findFileExtension(File file) {
		return (file == null || file.isDirectory()) ? null : findFileExtension(file.getName());
	}
	
	public static String findFileExtension(String fileName) {
		if(fileName == null || fileName.length() == 0)
			return null;
		int indexDot = fileName.lastIndexOf('.');
		if(indexDot < 0 || indexDot >= fileName.length()-1)
			return null;
		return fileName.substring(indexDot + 1);
	}
	
	public static String findFileName(File file) {
		return (file == null || file.isDirectory()) ? null : findFileName(file.getName());
	}
	
	public static String findFileName(String fileName) {
		if(fileName == null || fileName.length() == 0)
			return null;
		int indexDot = fileName.lastIndexOf('.');
		if(indexDot <= 0)
			return fileName;
		return fileName.substring(0, indexDot);
	}
	
	public static String getContentTypeOfFile(File file) throws IOException {
		return getContentTypeOfFile(file.getName());
	}
	
	public static String getContentTypeOfFile(String fileName) throws IOException {
		if(MimeType.MIME_TYPES.isEmpty())
			MimeType.read();
		return MimeType.contentTypeOfFileExtension(findFileExtension(fileName));
	}
	
	public static String getContentType(String fileExtension) throws IOException {
		if(MimeType.MIME_TYPES.isEmpty())
			MimeType.read();
		return MimeType.contentTypeOfFileExtension(fileExtension);
	}
}
