package com.vasnabilisim.io;

import java.io.IOException;
import java.util.TreeMap;

/**
 * @author Menderes Fatih GUVEN
 */
public class MimeType {

	static final TreeMap<String, MimeType> MIME_TYPES = new TreeMap<>();

	public static void register(MimeType mimeType) {
		MIME_TYPES.put(mimeType.fileExtension, mimeType);
	}
	
	public static MimeType ofFileExtension(String fileExtension) {
		return MIME_TYPES.get(fileExtension);
	}
	
	public static String contentTypeOfFileExtension(String fileExtension) {
		MimeType mimeType = MIME_TYPES.get(fileExtension);
		return mimeType == null ? "unknown" : mimeType.contentType;
	}
	
	public static void read() throws IOException {
		CsvReader reader = new CsvReader(MimeType.class.getResourceAsStream("mime-types.csv"));
		try {
			String[] values = null;
			while( (values = reader.readLine()) != null) {
				if(values.length != 3 || values[2] == null)
					continue;
				int index = values[2].indexOf('&');
				if(index < 0)
					register(new MimeType(values[0], values[1], values[2]));
				else {
					register(new MimeType(values[0], values[1], values[2].substring(0, index)));
					register(new MimeType(values[0], values[1], values[2].substring(index+1)));
				}
			}
		} catch (Exception e) {
			String message = String.format(
					"Unexpected exception reading file [mime-types.cs] at line [%d]", 
					reader.getCurrentLineNumber()
			);
			throw new IOException(message, e);
		} finally {
			try { reader.close(); } catch (IOException e) { }
		}
	}
	
	String name;
	String contentType;
	String fileExtension;
	
	public MimeType(String name, String contentType, String fileExtension) {
		this.name = name;
		this.contentType = contentType;
		this.fileExtension = fileExtension;
	}
	
	@Override
	public String toString() {
		return name + "," + contentType + "," + fileExtension;
	}

	public String getName() {
		return name;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public String getFileExtension() {
		return fileExtension;
	}
}
