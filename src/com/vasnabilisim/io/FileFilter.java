package com.vasnabilisim.io;

import java.io.File;
import java.util.ArrayList;

import com.vasnabilisim.util.Pair;

/**
 * @author Menderes Fatih GUVEN
 */
public class FileFilter implements java.io.FileFilter, java.io.FilenameFilter{

	private ArrayList<Pair<String, String>> extdescPairList = null;

	/**
	 * Constructs a list of acceptable file extensions and descriptions. 
	 * Ex : new FileFilter("jar", "Java Archive", "doc", "Microsoft Office Word")
	 * @param extdesc
	 */
	public FileFilter(String... extdesc) {
		if(extdesc == null || extdesc.length == 0)
			throw new IllegalArgumentException("Illegal argument extdesc. Connot be null or empty.");
		if(extdesc.length % 2 == 1)
			throw new IllegalArgumentException("Illegal argument extdesc. Number or arguments must be even.");
		int count = extdesc.length / 2;
		extdescPairList = new ArrayList<>(count);
		for(int index = 0; index < count; ++index)
			extdescPairList.add(new Pair<String, String>(extdesc[index * 2], extdesc[index * 2 + 1]));
	}
	
	@Override
	public boolean accept(File dir, String name) {
		return isExtension(IOUtil.findFileExtension(name));
	}

	@Override
	public boolean accept(File pathname) {
		return isExtension(IOUtil.findFileExtension(pathname));
	}

	/**
	 * Is the given extension one of the supported extensions.
	 * @param extension
	 * @return
	 */
	protected boolean isExtension(String extension) {
		for(Pair<String, String> pair : extdescPairList) {
			if(pair.getFirst() == null) {
				if(extension == null)
					return true;
				return false;
			}
			if(pair.getFirst().equals(extension))
				return true;
		}
		return false;
	}
}
