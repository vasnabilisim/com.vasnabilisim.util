package com.vasnabilisim.util;

import java.io.Serializable;

/**
 * Represents one object.
 * 
 * @author Menderes Fatih GÜVEN
 */
public class Single<First> implements Serializable {
	private static final long serialVersionUID = -2957513920852192367L;

	protected First first;

	/**
	 * Default constuctor.
	 */
	public Single() {
	}

	/**
	 * Value constructor.
	 * 
	 * @param first
	 */
	public Single(First first) {
		this.first = first;
	}

	/**
	 * Returns the fist value.
	 * 
	 * @return
	 */
	public First getFirst() {
		return first;
	}

	/**
	 * Sets the first value.
	 * 
	 * @param first
	 */
	public void setFirst(First first) {
		this.first = first;
	}

	/**
	 * Is given single equal to this.
	 * 
	 * @param other
	 * @return
	 */
	public boolean equals(Single<First> other) {
		return new EqualsBuilder().append(this.first, other.first).toBoolean();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Single)
			return equals((Single<First>) obj);
		return false;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return first == null ? "null" : first.toString();
	}
}
