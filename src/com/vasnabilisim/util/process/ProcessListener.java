package com.vasnabilisim.util.process;

/**
 * @author Menderes Fatih GUVEN
 */
public interface ProcessListener {

	void processStateChanges(ProcessEvent e);
}
