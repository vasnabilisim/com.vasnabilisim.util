package com.vasnabilisim.util.process;

import java.awt.Component;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

/**
 * @author Menderes Fatih GUVEN
 */
public final class Processer {

	static IProcesserHandlerFactory processerHandlerFactory = IProcesserHandlerFactory.Idle;
	
	public static IProcesserHandlerFactory getProcesserHandlerFactory() {
		return processerHandlerFactory;
	}
	
	public static void setProcesserHandlerFactory(IProcesserHandlerFactory processerHandlerFactory) {
		Processer.processerHandlerFactory = processerHandlerFactory;
	}
	
	static Object LOCK = new Object();
	
	boolean cancelOnFail;
	Component comp;
	IProcess<?>[] processes;
	ExecutorService executorService;
	boolean finished = false;
	boolean result = false;
	IProcesserHandler processerHandler = null;
	
	private Processer(Component comp, boolean cancelOnFail, IProcess<?>... processes) {
		this.cancelOnFail = cancelOnFail;
		this.comp = comp;
		this.processes = processes;
		executorService = Executors.newSingleThreadExecutor();
	}
	
	public int getProcessCount() {
		return processes.length;
	}
	
	public IProcess<?>[] getProcesses() {
		return processes;
	}
	
	public IProcess<?> getProcess(int index) {
		return processes[index];
	}
	
	public boolean isFinished() {
		return finished;
	}
	
	private boolean process() {
		startProcessThread();
		long time = System.currentTimeMillis();
		while(!finished) {
			if(System.currentTimeMillis() - time > 1000L)
				break;
			try { Thread.sleep(100); } catch (InterruptedException e) {}
		}
		synchronized (LOCK) {
			if(!finished)
				processerHandler = processerHandlerFactory.create(this);
		}
		if(!finished) {
			boolean handleResult = processerHandler.processHandle();
			executorService.shutdown();
			if(finished)
				return result;
			cancelProcessThread();
			return handleResult;
		}
		else if(!result) {
			//We couldn't show handler but some error occured
			if(processerHandler == null)
				processerHandler = processerHandlerFactory.create(this);
			processerHandler.processHandle();
		}
		/*
		for(IProcess<?> process : processes)
			if(process.getState() != ProcessState.Succeded)
				return false;
		*/
		executorService.shutdown();
		return result;
	}
	

	@SafeVarargs
	public static <R> boolean process(Component comp, IProcess<R>... processes) {
		return process(comp, true, processes);
	}

	@SafeVarargs
	public static <R> boolean process(Component comp, boolean cancelOnFail, IProcess<R>... processes) {
		Processer processer = new Processer(comp, cancelOnFail, processes);
		return processer.process();
	}

	<R> boolean process(IProcess<R> process) {
		return process(ProcessFutureTask.get(process));
	}

	<R> boolean process(ProcessFutureTask<R> task) {
		task.getProcess().setState(ProcessState.Running);
		executorService.execute(task);
		try {
			task.get(task.getProcess().getTime(), task.getProcess().getTimeUnit());
			return task.getProcess().getState() == ProcessState.Succeded;
		} catch (Throwable e) { //catch (InterruptedException | ExecutionException | TimeoutException e) {
			return handleException(task.getProcess(), e);
		}
	}
	
	<R> boolean handleException(IProcess<R> process, Throwable e) {
		Throwable ex = e;
		while(ex != null) {
			if(ex instanceof CancellationException) {
				process.setState(ProcessState.Canceled);
				process.setException(ex);
				process.processCanceled((CancellationException) ex);
				return false;
			}
			if(ex instanceof InterruptedException) {
				//TODO not sure to cancel.
				process.setState(ProcessState.Canceled);
				CancellationException ce = new CancellationException(ex.getMessage());
				ce.addSuppressed(ex);
				process.setException(ce);
				process.processCanceled(ce);
				return false;
			}
			if(ex instanceof TimeoutException) {
				process.setState(ProcessState.TimedOut);
				process.setException(ex);
				process.processTimedOut((TimeoutException) ex);
				return false;
			}
			ex = ex.getCause();
		}
		process.setState(ProcessState.Failed);
		process.setException(e);
		process.processFailed(e);
		return false;
	}
	
	Thread processThread = null;
	void startProcessThread() {
		processThread = new Thread() {
			public void run() {
				boolean r = true;
				for(IProcess<?> process : processes) {
					synchronized (LOCK) {
						if(processThread == null) {
							result = false;
							finished = true;
							if(processerHandler != null)
								processerHandler.processFinished(false);
						}
					}
					if(finished) return;
					r &= process(process);
					synchronized (LOCK) {
						if(cancelOnFail & !r) {
							result = false;
							finished = true;
							if(processerHandler != null)
								processerHandler.processFinished(false);
						}
					}
					if(finished) return;
				}
				synchronized (LOCK) {
					result = r;
					finished = true;
					if(processerHandler != null)
						processerHandler.processFinished(result);
				}
			}
		};
		processThread.setName("Processer-" + UUID.randomUUID());
		processThread.start();
	}
	
	void cancelProcessThread() {
		synchronized (LOCK) {
			System.out.println("190");
			if(processThread != null)
				processThread = null;
		}
		System.out.println("194");
	}
}
