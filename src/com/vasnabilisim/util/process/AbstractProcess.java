package com.vasnabilisim.util.process;

import java.util.LinkedList;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractProcess<R> implements IProcess<R> {

	protected ProcessState state;
	protected String info;
	protected Throwable exception;
	
	protected LinkedList<ProcessListener> listeners = new LinkedList<>();
	
	protected AbstractProcess() {
		state = ProcessState.Waiting;
	}

	protected AbstractProcess(String info) {
		this.info = info;
		state = ProcessState.Waiting;
	}

	@Override
	public String getInfo() {
		return info;
	}
	
	@Override
	public ProcessState getState() {
		return state;
	}
	
	@Override
	public void setState(ProcessState state) {
		this.state = state;
		fireProcessEvent();
	}
	
	@Override
	public Throwable getException() {
		return exception;
	}
	
	@Override
	public void setException(Throwable e) {
		this.exception = e;
	}
	
	@Override
	public long getTime() {
		//return 5;
		return 5000;
	}

	@Override
	public TimeUnit getTimeUnit() {
		return TimeUnit.SECONDS;
	}
	
	@Override
	public boolean cancelable() {
		return true;
	}

	@Override
	public void processSucceded(R value) throws Throwable {
		//DO NOTHING
	}

	@Override
	public void processCanceled(CancellationException e) {
		//DO NOTHING
	}

	@Override
	public void processFailed(Throwable e) {
		
	}

	@Override
	public void processTimedOut(TimeoutException e) {
	}
	
	@Override
	public void addProcessListener(ProcessListener l) {
		listeners.add(l);
	}
	
	@Override
	public void removeProcessListener(ProcessListener l) {
		listeners.remove(l);
	}
	
	protected void fireProcessEvent(ProcessEvent e) {
		for(ProcessListener l : listeners)
			l.processStateChanges(e);
	}
	
	public void fireProcessEvent() {
		fireProcessEvent(new ProcessEvent(this));
	}

	@SuppressWarnings("unchecked")
	public static <R> IProcess<R> idle() {
		return (IProcess<R>) IProcess.Idle;
	}
}
