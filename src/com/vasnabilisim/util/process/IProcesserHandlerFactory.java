package com.vasnabilisim.util.process;

/**
 * @author Menderes Fatih GUVEN
 */
public interface IProcesserHandlerFactory {

	IProcesserHandler create(Processer processer);
	
	public static final IdleProcesserHandlerFactory Idle = new IdleProcesserHandlerFactory();
	public static class IdleProcesserHandlerFactory implements IProcesserHandlerFactory {
		@Override
		public IProcesserHandler create(Processer processer) {
			return IProcesserHandler.Idle;
		}
		
	}
}
