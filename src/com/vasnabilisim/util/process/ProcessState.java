package com.vasnabilisim.util.process;

/**
 * @author Menderes Fatih GUVEN
 */
public enum ProcessState {

	Waiting,
	Running, 
	Succeded, 
	Failed, 
	Canceled, 
	TimedOut;
}
