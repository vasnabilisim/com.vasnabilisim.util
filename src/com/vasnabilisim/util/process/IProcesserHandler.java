package com.vasnabilisim.util.process;

/**
 * @author Menderes Fatih GUVEN
 */
public interface IProcesserHandler {

	boolean processHandle();

	void processFinished(boolean success);

	public static IdleProcesserHandler Idle = new IdleProcesserHandler();
	public static class IdleProcesserHandler implements IProcesserHandler {

		@Override
		public boolean processHandle() {
			return true;
		}

		@Override
		public void processFinished(boolean success) {
		}
	}
}
