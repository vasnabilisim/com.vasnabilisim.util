package com.vasnabilisim.util.process;

import java.util.concurrent.FutureTask;

/**
 * @author Menderes Fatih GUVEN
 */
public class ProcessFutureTask<V> extends FutureTask<V> {

	protected ProcessCallable<V> callable;

	protected ProcessFutureTask(ProcessCallable<V> callable) {
		super(callable);
		this.callable = callable;
	}
	
	public ProcessCallable<V> getCallable() {
		return callable;
	}
	
	public IProcess<V> getProcess() {
		return callable.process;
	}
	
	public boolean cancel() {
		if(getProcess().cancelable())
			return cancel(true);
		return false;
	}
	
	public static <V> ProcessFutureTask<V> get(IProcess<V> process) {
		return new ProcessFutureTask<>(new ProcessCallable<>(process));
	}
}
