package com.vasnabilisim.util.process;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author Menderes Fatih GUVEN
 */
public interface IProcess<R> {

	String getInfo();
	
	long getTime();
	
	TimeUnit getTimeUnit();
	
	boolean cancelable();
	
	R execute() throws Throwable;
	
	ProcessState getState();
	
	void setState(ProcessState state);

	Throwable getException();

	void setException(Throwable e);
	
	void processSucceded(R value) throws Throwable;
	
	void processFailed(Throwable e);
	
	void processCanceled(CancellationException e);
	
	void processTimedOut(TimeoutException e);
	
	void addProcessListener(ProcessListener l);
	
	void removeProcessListener(ProcessListener l);
	
	IProcess<Void> Idle = new AbstractProcess<Void>() {

		@Override
		public Void execute() throws Throwable {
			try {Thread.sleep(20000);} catch (InterruptedException e) {}
			return null;
		}
	};
}
