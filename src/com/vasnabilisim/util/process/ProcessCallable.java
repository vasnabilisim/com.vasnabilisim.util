package com.vasnabilisim.util.process;

import java.util.concurrent.Callable;

public class ProcessCallable<R> implements Callable<R> {

	protected IProcess<R> process;
	
	public ProcessCallable(IProcess<R> process) {
		this.process = process;
	}
	
	public IProcess<R> getProcess() {
		return process;
	}

	@Override
	public R call() throws Exception {
		try {
			R result = call0();
			process.setState(ProcessState.Succeded);
			process.processSucceded(result);
			return result;
		} catch (Throwable e) {
			process.setState(ProcessState.Failed);
			process.setException(e);
			process.processFailed(e);
			return null;
		}
	}

	R call0() throws Throwable {
		return process.execute();
	}
}