package com.vasnabilisim.util.process;

import java.util.EventObject;

/**
 * @author Menderes Fatih GUVEN
 */
public class ProcessEvent extends EventObject {
	private static final long serialVersionUID = -9077858922346951695L;

	/**
	 * @param source
	 */
	public ProcessEvent(IProcess<?> source) {
		super(source);
	}

	public IProcess<?> getProcess() {
		return (IProcess<?>) getSource();
	}
}
