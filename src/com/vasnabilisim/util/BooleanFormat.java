package com.vasnabilisim.util;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

/**
 * BooleanFormat
 * @author Menderes Fatih GUVEN
 */
public class BooleanFormat extends Format {
	private static final long serialVersionUID = 8053214302332798557L;

	private String pattern = null;
	private String falseValue = "";
	private String trueValue = "";
	
	/**
	 * Default constructor.
	 */
	public BooleanFormat() {
		this("0:1");
	}
	
	public BooleanFormat(String pattern) {
		super();
		setPattern(pattern);
	}

	/**
	 * Returns the pattern.
	 * @return
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * Sets the pattern.
	 * @param pattern
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
		
		int index = pattern.indexOf(':');
		if(index < 0 || index >= pattern.length()-1)
			throw new IllegalArgumentException("Illegal pattern " + pattern + "");
		falseValue = pattern.substring(0, index);
		trueValue = pattern.substring(index+1);
	}

	/**
	 * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer, java.text.FieldPosition)
	 */
	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		if(!(obj instanceof Boolean))
			throw new IllegalArgumentException("Formats only boolean.");
		return format((Boolean)obj, toAppendTo, pos);
	}

	public StringBuffer format(Boolean obj, StringBuffer toAppendTo, FieldPosition pos) {
		pos.setBeginIndex(0);
		if(obj) {
			toAppendTo.append(trueValue);
			pos.setEndIndex(trueValue.length());
		}
		else {
			toAppendTo.append(falseValue);
			pos.setEndIndex(falseValue.length());
		}
		return toAppendTo;
	}

	/**
	 * @see java.text.Format#parseObject(java.lang.String, java.text.ParsePosition)
	 */
	@Override
	public Object parseObject(String source, ParsePosition pos) {
		return parseBoolean(source, pos);
	}

	public Boolean parseBoolean(String source, ParsePosition pos) {
		if(source.startsWith(trueValue)) {
			pos.setIndex(trueValue.length());
			return Boolean.TRUE;
		}
		if(source.startsWith(falseValue)) {
			pos.setIndex(falseValue.length());
			return Boolean.FALSE;
		}
		pos.setErrorIndex(0);
		return null;
	}
}
