package com.vasnabilisim.util;

import java.util.Map;

/**
 * @author Menderes Fatih GUVEN
 */
public class MapKeyValueAppender<K, V> implements KeyValueAppender<K, V> {

	protected Map<K, V> source;
	
	public MapKeyValueAppender(Map<K, V> source) {
		this.source = source;
	}
	
	@Override
	public MapKeyValueAppender<K, V> append(K key, V value) {
		source.put(key, value);
		return this;
	}

}
