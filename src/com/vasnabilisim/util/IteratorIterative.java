package com.vasnabilisim.util;

import java.util.Iterator;

/**
 * Iterator Iterative. 
 * @author Menderes Fatih GUVEN
 */
public class IteratorIterative<T> implements Iterative<T> {

	/**
	 * Default constructor.
	 */
	public IteratorIterative(Iterator<T> iterator) {
		this.iterator = iterator;
	}

	/**
	 * Collection constructor.
	 * @param c
	 */
	public IteratorIterative(Iterable<T> c) {
		this(c.iterator());
	}

	private Iterator<T> iterator = null;
	private T current = null;

	/**
	 * @see com.vasnabilisim.util.Iterative#start()
	 */
	@Override
	public void start() {
		throw new IllegalStateException("Cannot be started.");
	}

	/**
	 * @see com.vasnabilisim.util.Iterative#stop()
	 */
	@Override
	public void stop() {
		iterator = null;
		current = null;
	}

	/**
	 * @see com.vasnabilisim.util.Iterative#current()
	 */
	@Override
	public T current() {
		if(iterator == null)
			throw new IllegalStateException("Iterative not active.");
		return current;
	}

	/**
	 * @see com.vasnabilisim.util.Iterative#hasNext()
	 */
	@Override
	public boolean hasNext() {
		if(iterator == null)
			throw new IllegalStateException("Iterative not active.");
		return iterator.hasNext();
	}

	/**
	 * @see com.vasnabilisim.util.Iterative#next()
	 */
	@Override
	public T next() {
		if(iterator == null)
			throw new IllegalStateException("Iterative not active.");
		current = iterator.next();
		return current;
	}
}
