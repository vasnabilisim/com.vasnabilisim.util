package com.vasnabilisim.util;

import java.io.Serializable;

/**
 * @author Menderes Fatih GUVEN
 */
public class Tree<T> implements Serializable {
	private static final long serialVersionUID = 7374683807954454608L;

	protected TreeNode<T> root = new TreeNode<>();
	
	public Tree() {
	}

	public TreeNode<T> getRoot() {
		return root;
	}
	
	public void setRoot(TreeNode<T> root) {
		this.root = root == null ? new TreeNode<T>() : root;
	}
}
