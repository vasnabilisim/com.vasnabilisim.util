package com.vasnabilisim.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Menderes Fatih GUVEN
 */
public class ArrayIterator<T> implements Iterator<T> {

	T[] source;
	int nextIndex;
	
	public ArrayIterator(T[] source) {
		this.source = source;
		nextIndex = 0;
	}
	
	public void reset() {
		nextIndex = 0;
	}

	@Override
	public boolean hasNext() {
		return nextIndex < source.length;
	}

	@Override
	public T next() {
		if(nextIndex >= source.length)
			throw new NoSuchElementException();
		return source[nextIndex++];
	}

}
