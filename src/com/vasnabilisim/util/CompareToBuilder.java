package com.vasnabilisim.util;

/**
 * CompareToBuilder
 * @author Menderes Fatih GUVEN
 */
public class CompareToBuilder {

	private int result = 0;
	
	public <T extends Comparable<T>> CompareToBuilder append(T o1, T o2) {
		if(result == 0)
			result = o1 == null ? (o2 == null ? 0 : -1) : (o2 == null ? 1 : o1.compareTo(o2));
		return this;
	}
	
	public CompareToBuilder append(boolean o1, boolean o2) {
		if(result == 0)
			result = Boolean.compare(o1, o2);
		return this;
	}
	
	public CompareToBuilder append(byte o1, byte o2) {
		if(result == 0)
			result = Byte.compare(o1, o2);
		return this;
	}
	
	public CompareToBuilder append(char o1, char o2) {
		if(result == 0)
			result = Character.compare(o1, o2);
		return this;
	}
	
	public CompareToBuilder append(int o1, int o2) {
		if(result == 0)
			result = Integer.compare(o1, o2);
		return this;
	}
	
	public CompareToBuilder append(long o1, long o2) {
		if(result == 0)
			result = Long.compare(o1, o2);
		return this;
	}
	
	public CompareToBuilder append(float o1, float o2) {
		if(result == 0)
			result = Float.compare(o1, o2);
		return this;
	}
	
	public CompareToBuilder append(double o1, double o2) {
		if(result == 0)
			result = Double.compare(o1, o2);
		return this;
	}
	
	public int toInt() {
		return result;
	}
}
