package com.vasnabilisim.util;

import java.io.Serializable;

/**
 * Represents three objects.
 * 
 * @author Menderes Fatih GUVEN
 */
public class Triple<First, Second, Third> extends Pair<First, Second> implements Serializable {
	private static final long serialVersionUID = -3653955138849171837L;

	protected Third third;

	/**
	 * Default constructor.
	 */
	public Triple() {
		super();
	}

	/**
	 * Value constructor.
	 * 
	 * @param first
	 * @param second
	 * @param third
	 */
	public Triple(First first, Second second, Third third) {
		super(first, second);
		this.third = third;
	}

	/**
	 * Returns the third value.
	 * 
	 * @return
	 */
	public Third getThird() {
		return third;
	}

	/**
	 * Sets the third value.
	 * 
	 * @param third
	 */
	public void setThird(Third third) {
		this.third = third;
	}

	/**
	 * Is given triple equal to this.
	 * 
	 * @param other
	 * @return
	 */
	public boolean equals(Triple<First, Second, Third> other) {
		return new EqualsBuilder().
				append(this.first, other.first).
				append(this.second, other.second).
				append(this.third, other.third).
				toBoolean();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Triple)
			return equals((Triple<First, Second, Third>) obj);
		return false;
	}

	/**
	 * @see com.vasnabilisim.util.Pair#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + "-" + (third == null ? "null" : third.toString());
	}
}
