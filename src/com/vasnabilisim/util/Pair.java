package com.vasnabilisim.util;

import java.io.Serializable;

/**
 * Pair class.
 * 
 * @author Menderes Fatih GUVEN
 */
public class Pair<First, Second> extends Single<First> implements Serializable {
	private static final long serialVersionUID = 5729917454742805550L;

	protected Second second;

	/**
	 * Default constructor.
	 */
	public Pair() {
	}

	/**
	 * Value constructor.
	 * 
	 * @param first
	 * @param second
	 */
	public Pair(First first, Second second) {
		super(first);
		this.second = second;
	}

	/**
	 * Returns the second.
	 * 
	 * @return
	 */
	public Second getSecond() {
		return second;
	}

	/**
	 * Sets the second.
	 * 
	 * @param second
	 */
	public void setSecond(Second second) {
		this.second = second;
	}

	/**
	 * Is given pair equal to this.
	 * 
	 * @param other
	 * @return
	 */
	public boolean equals(Pair<First, Second> other) {
		return new EqualsBuilder().append(this.first, other.first).append(this.second, other.second).toBoolean();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Pair)
			return equals((Pair<First, Second>) obj);
		return false;
	}

	/**
	 * @see com.vasnabilisim.util.Single#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + "-" + (second == null ? "null" : second.toString());
	}

}
