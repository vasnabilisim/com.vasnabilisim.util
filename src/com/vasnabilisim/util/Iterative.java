package com.vasnabilisim.util;

import java.util.Iterator;

/**
 * Iterative interface.
 * @author Menderes Fatih GUVEN
 */
public interface Iterative<T> extends Iterator<T>{

	/**
	 * Starts the iteration.
	 */
	void start();
	
	/**
	 * Stops the iteration.
	 */
	void stop();

	/**
	 * Returns the current object of the iteration.
	 * 
	 * @return
	 */
	T current();

	/**
	 * @see java.util.Iterator#hasNext()
	 */
	boolean hasNext();

	/**
	 * @see java.util.Iterator#next()
	 */
	T next();

}
