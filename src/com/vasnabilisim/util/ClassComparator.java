package com.vasnabilisim.util;

import java.util.Comparator;

/**
 * Comparator of Class types. Simply compares the names of the classes.
 * 
 * @author Menderes Fatih GUVEN
 */
public class ClassComparator implements Comparator<Class<?>> {

	/**
	 * @see java.util.Comparator#compare(T, T)
	 */
	public int compare(Class<?> class1, Class<?> class2) {
		if (class1 == null) {
			if (class2 == null)
				return 0;
			return -1;
		}
		if (class2 == null)
			return 1;
		return class1.getName().compareTo(class2.getName());
	}

	/**
	 * @see java.util.Comparator#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return obj instanceof ClassComparator;
	}
}
