package com.vasnabilisim.util;

import java.io.Serializable;

/**
 * Represents four objects.
 * 
 * @author Menderes Fatih GUVEN
 */
public class Quadruple<First, Second, Third, Fourth> extends Triple<First, Second, Third> implements Serializable {
	private static final long serialVersionUID = 6772303410092727394L;

	protected Fourth fourth;

	/**
	 * Default constructor.
	 */
	public Quadruple() {
	}

	/**
	 * Value constructor.
	 * 
	 * @param first
	 * @param second
	 * @param third
	 * @param fourth
	 */
	public Quadruple(First first, Second second, Third third, Fourth fourth) {
		super(first, second, third);
		this.fourth = fourth;
	}

	/**
	 * Returns the fourth value.
	 * 
	 * @return
	 */
	public Fourth getFourth() {
		return fourth;
	}

	/**
	 * Sets the fourth value.
	 * 
	 * @param fourth
	 */
	public void setFourth(Fourth fourth) {
		this.fourth = fourth;
	}

	/**
	 * Is given quadruple equal to this.
	 * 
	 * @param other
	 * @return
	 */
	public boolean equals(Quadruple<First, Second, Third, Fourth> other) {
		return new EqualsBuilder().
				append(this.first, other.first).
				append(this.second, other.second).
				append(this.third, other.third).
				append(this.fourth, other.fourth).
				toBoolean();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Quadruple)
			return equals((Quadruple<First, Second, Third, Fourth>) obj);
		return false;
	}

	/**
	 * @see com.vasnabilisim.util.Triple#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + "-" + (fourth == null ? "null" : fourth.toString());
	}
}
