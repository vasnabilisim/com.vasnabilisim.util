package com.vasnabilisim.util;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;

/**
 * @author Menderes Fatih GUVEN
 */
public class MessageList extends AbstractList<Message> implements Serializable {
	private static final long serialVersionUID = 7531216801376162871L;

	MessageType type = MessageType.Message;
	ArrayList<Message> source = new ArrayList<>();
	
	public MessageList() {
	}
	
	public MessageType getType() {
		return type;
	}

	@Override
	public void clear() {
		type = MessageType.Message;
		source.clear();
	}
	
	public int size() {
		return source.size();
	}
	
	@Override
	public Message get(int index) {
		return source.get(index);
	}
	
	@Override
	public void add(int index, Message element) {
		if(element.type != MessageType.Question && element.type.ordinal() > this.type.ordinal())
			this.type = element.type;
		source.add(index, element);
	}
	
	/**
	 * Returns the maximum message type level of contained messages.
	 * @param type
	 * @param message
	 * @return
	 */
	public Message add(MessageType type, String message) {
		Message m = new Message(type, message);
		add(m);
		return m;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder builder = new StringBuilder(source.size() * 65);
		for(int index=0; index<source.size(); ++index) {
			if(index > 0)
				builder.append(System.lineSeparator());
			builder.append(source.get(index).getMessage());
		}
		return builder.toString();
	}

	public Message info(String message) {
		return add(MessageType.Info, message);
	}

	public Message warning(String message) {
		return add(MessageType.Warning, message);
	}

	public Message error(String message) {
		return add(MessageType.Error, message);
	}

	public Message fatal(String message) {
		return add(MessageType.Fatal, message);
	}

	/*
	public Message add(BaseException e) {
		Message m = Message.get(e);
		messages.add(m);
		return m;
	}

	public Message add(String message, BaseException e) {
		Message m = Message.get(message, e);
		messages.add(m);
		return m;
	}

	public Message add(Throwable e) {
		Message m = Message.get(e);
		messages.add(m);
		return m;
	}

	public Message add(String message, Throwable e) {
		Message m = Message.get(message, e);
		messages.add(m);
		return m;
	}
	*/
}
