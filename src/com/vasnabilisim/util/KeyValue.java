package com.vasnabilisim.util;

import java.io.Serializable;

/**
 * Basic property class. 
 * Contains key value pair.
 * @author Menderes Fatih GUVEN
 */
public class KeyValue implements com.vasnabilisim.util.Cloneable<KeyValue>, java.lang.Cloneable, Comparable<KeyValue>, Serializable {
	private static final long serialVersionUID = -4724784836343962375L;

	/**
	 * Name of the property.
	 */
	protected String key = null;
	
	/**
	 * Value of the property.
	 */
	protected String value = null;
	
	/**
	 * Default constructor.
	 */
	public KeyValue() {
	}

	/**
	 * Name constructor.
	 * @param key
	 */
	public KeyValue(String key) {
		this.key = key;
	}

	/**
	 * Value constructor.
	 * @param key
	 * @param value
	 */
	public KeyValue(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	public KeyValue(KeyValue source) {
		this.key = source.key;
		this.value = source.value;
	}
	
	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public KeyValue clone() {
		return cloneObject();
	}

	/**
	 * @see com.vasnabilisim.util.Cloneable#cloneObject()
	 */
	@Override
	public KeyValue cloneObject() {
		return new KeyValue(this);
	}

	/**
	 * Returns the key of the property.
	 * @return
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the key of the property.
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Returns the value of the property.
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value of the property.
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return key + "=" + value;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj == this)
			return true;
		if(!(obj instanceof KeyValue))
			return false;
		KeyValue other = (KeyValue)obj;
		return new EqualsBuilder().appendNotNull(this.key, other.key).appendNotNull(this.value, other.value).toBoolean();
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(KeyValue other) {
		if(other == null)
			return 1;
		if(other == this)
			return 0;
		return new CompareToBuilder().append(this.key, other.key).toInt();
	}
}
