package com.vasnabilisim.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Menderes Fatih GUVEN
 */
public interface Converter<F, T> {

	/**
	 * Converts from F to T
	 * @param from
	 * @return
	 */
	T convert(F from);
	
	/**
	 * Converts all items of given list and return a new list of converted items.
	 * @param fromList
	 * @return
	 */
	default List<T> convertAll(List<F> fromList) {
		List<T> toList = new ArrayList<T>(fromList.size());
		for(F from : fromList)
			toList.add(this.convert(from));
		return null;
	}

	/**
	 * Converts all items of given set and return a new set of converted items.
	 * @param fromSet
	 * @return
	 */
	default Set<T> convertAll(Set<F> fromSet) {
		Set<T> toSet = new HashSet<T>(fromSet.size());
		for(F from : fromSet)
			toSet.add(this.convert(from));
		return null;
	}
}
