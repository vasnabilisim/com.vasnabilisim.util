package com.vasnabilisim.util;

import java.io.Serializable;

/**
 * @author Menderes Fatih GUVEN
 */
public class Message implements Serializable {
	private static final long serialVersionUID = 6562960130984934505L;

	MessageType type;
	String message;
	Throwable exception;

	public Message(MessageType type, String message) {
		this.type = type;
		this.message = message;
	}
	
	public Message(MessageType type, String message, Throwable exception) {
		this.type = type;
		this.message = message;
		this.exception = exception;
	}
	/*
	public Message(BaseException exception) {
		this.type = exception.getType();
		this.message = exception.getMessage();
		this.exception = exception;
	}
	
	public Message(String message, BaseException exception) {
		this.type = exception.getType();
		this.message = message;
		this.exception = exception;
	}
	
	public Message(String message, Throwable exception) {
		if(exception instanceof BaseException)
			this.type = ((BaseException)exception).getType();
		this.message = message;
		this.exception = exception;
	}
	
	public Message(Throwable exception) {
		if(exception instanceof BaseException)
			this.type = ((BaseException)exception).getType();
		this.message = exception.getMessage();
		this.exception = exception;
	}
	*/
	@Override
	public String toString() {
		return type + " - " + message;
	}
	
	public MessageType getType() {
		return type;
	}
	
	public void setType(MessageType type) {
		this.type = type;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Throwable getException() {
		return exception;
	}
	
	public void setException(Throwable exception) {
		this.exception = exception;
	}
	
	public static Message info(String message) {
		return new Message(MessageType.Info, message);
	}
	
	public static Message warning(String message) {
		return new Message(MessageType.Warning, message);
	}
	
	public static Message error(String message) {
		return new Message(MessageType.Error, message);
	}
	
	public static Message fatal(String message) {
		return new Message(MessageType.Fatal, message);
	}

	/*
	public static Message get(BaseException e) {
		return new Message(e);
	}

	public static Message get(String message, BaseException e) {
		return new Message(message, e);
	}

	public static Message get(Throwable e) {
		return new Message(e);
	}

	public static Message get(String message, Throwable e) {
		return new Message(message, e);
	}
	*/
}
