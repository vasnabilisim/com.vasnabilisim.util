package com.vasnabilisim.util;

/**
 * MessageType
 * Used for exception handling and logging.
 * 
 * @author Menderes Fatih GUVEN
 */
public enum MessageType {
	Message, 
	Info, 
	Warning, 
	Error, 
	Fatal, 
	Question; 
}
