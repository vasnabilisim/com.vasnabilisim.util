package com.vasnabilisim.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * CloneableArrayList
 * @author Menderes Fatih GUVEN
 */
public class CloneableArrayList<T extends Cloneable<T>> extends ArrayList<T> 
implements Serializable, CloneableList<T>, java.lang.Cloneable
{
	private static final long serialVersionUID = -5221983659777630439L;

	/**
	 * Default constructor.
	 */
	public CloneableArrayList() {
		super();
	}
	
	/**
	 * Size constructor.
	 * @param size
	 */
	public CloneableArrayList(int size) {
		super(size);
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	public CloneableArrayList(Collection<T> source) {
		super(source.size());
		for(T item : source)
			this.add(item.cloneObject());
	}

	/**
	 * @see java.util.ArrayList#clone()
	 */
	@Override
	public Object clone() {
		return cloneObject();
	}

	/**
	 * @see com.vasnabilisim.util.Cloneable#cloneObject()
	 */
	@Override
	public CloneableArrayList<T> cloneObject() {
		return new CloneableArrayList<T>(this);
	}
}
