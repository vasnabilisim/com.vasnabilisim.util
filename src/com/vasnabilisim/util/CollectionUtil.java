package com.vasnabilisim.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Menderes Fatih GUVEN
 */
public final class CollectionUtil {

	private CollectionUtil() {
	}

	public static <Base, Sub extends Base> List<Sub> upCast(List<Base> source) {
		return new UpCastList<Base, Sub>(source);
	}
	
	static class UpCastListIterator<Base, Sub extends Base> extends UpCastIterator<Base, Sub> implements ListIterator<Sub> {
		ListIterator<Base> source;
		UpCastListIterator(ListIterator<Base> source) {
			super(source);
			this.source = source;
		}
		public boolean hasPrevious() { return source.hasPrevious(); }
		@SuppressWarnings("unchecked")
		public Sub previous() { return (Sub)source.previous(); }
		public int nextIndex() { return source.nextIndex(); }
		public int previousIndex() { return source.previousIndex(); }
		public void remove() { throw new UnsupportedOperationException(); }
		public void set(Sub e) { throw new UnsupportedOperationException(); }
		public void add(Sub e) { throw new UnsupportedOperationException(); }
	}
	
	static class UpCastList<Base, Sub extends Base> extends UpCastCollection<Base, Sub> implements List<Sub> {
		List<Base> source;
		UpCastList(List<Base> source) {
			super(source);
			this.source = source;
		}
		public boolean addAll(int index, Collection<? extends Sub> c) { throw new UnsupportedOperationException(); }
		@SuppressWarnings("unchecked")
		public Sub get(int index) { return (Sub)source.get(index); }
		public Sub set(int index, Sub element) { throw new UnsupportedOperationException(); }
		public void add(int index, Sub element) { throw new UnsupportedOperationException(); }
		public Sub remove(int index) { throw new UnsupportedOperationException(); }
		public int indexOf(Object o) { return source.indexOf(o); }
		public int lastIndexOf(Object o) { return source.lastIndexOf(o); }
		public ListIterator<Sub> listIterator() { return new UpCastListIterator<Base, Sub>(source.listIterator()); }
		public ListIterator<Sub> listIterator(int index) { return new UpCastListIterator<Base, Sub>(source.listIterator(index)); }
		public List<Sub> subList(int fromIndex, int toIndex) { throw new UnsupportedOperationException(); }
	}

	public static <Base, Sub extends Base> Collection<Sub> upCast(Collection<Base> source) {
		return new UpCastCollection<Base, Sub>(source);
	}
	
	static class UpCastIterator<Base, Sub extends Base> implements Iterator<Sub> {
		Iterator<Base> source;
		UpCastIterator(Iterator<Base> source) {
			this.source = source;
		}
		public boolean hasNext() { return source.hasNext(); }
		@SuppressWarnings("unchecked")
		public Sub next() { return (Sub)source.next(); }
	}
	
	static class UpCastCollection<Base, Sub extends Base> implements Collection<Sub> {
		Collection<Base> source;
		UpCastCollection(Collection<Base> source) {
			this.source = source;
		}
		public int size() { return source.size(); }
		public boolean isEmpty() { return source.isEmpty(); }
		public boolean contains(Object o) { return source.contains(o); }
		public Iterator<Sub> iterator() { return new UpCastIterator<Base, Sub>(source.iterator()); }
		public Object[] toArray() { return source.toArray(); }
		public <E> E[] toArray(E[] a) { return source.toArray(a); }
		public boolean add(Sub e) { throw new UnsupportedOperationException(); }
		public boolean remove(Object o) { throw new UnsupportedOperationException(); }
		public boolean containsAll(Collection<?> c) { return source.containsAll(c); }
		public boolean addAll(Collection<? extends Sub> c) { throw new UnsupportedOperationException(); }
		public boolean removeAll(Collection<?> c) { throw new UnsupportedOperationException(); }
		public boolean retainAll(Collection<?> c) { throw new UnsupportedOperationException(); }
		public void clear() { throw new UnsupportedOperationException(); }
	}

	public static <Base, Sub extends Base> List<Base> downCast(List<Sub> source) {
		return new DownCastList<Base, Sub>(source);
	}
	
	static class DownCastListIterator<Base, Sub extends Base> extends DownCastIterator<Base, Sub> implements ListIterator<Base> {
		ListIterator<Sub> source;
		DownCastListIterator(ListIterator<Sub> source) {
			super(source);
			this.source = source;
		}
		public boolean hasPrevious() { return source.hasPrevious(); }
		public Base previous() { return source.previous(); }
		public int nextIndex() { return source.nextIndex(); }
		public int previousIndex() { return source.previousIndex(); }
		public void remove() { throw new UnsupportedOperationException(); }
		public void set(Base e) { throw new UnsupportedOperationException(); }
		public void add(Base e) { throw new UnsupportedOperationException(); }
	}
	
	static class DownCastList<Base, Sub extends Base> extends DownCastCollection<Base, Sub> implements List<Base> {
		List<Sub> source;
		DownCastList(List<Sub> source) {
			super(source);
			this.source = source;
		}
		public boolean addAll(int index, Collection<? extends Base> c) { throw new UnsupportedOperationException(); }
		public Sub get(int index) { return (Sub)source.get(index); }
		public Sub set(int index, Base element) { throw new UnsupportedOperationException(); }
		public void add(int index, Base element) { throw new UnsupportedOperationException(); }
		public Sub remove(int index) { throw new UnsupportedOperationException(); }
		public int indexOf(Object o) { return source.indexOf(o); }
		public int lastIndexOf(Object o) { return source.lastIndexOf(o); }
		public ListIterator<Base> listIterator() { return new DownCastListIterator<Base, Sub>(source.listIterator()); }
		public ListIterator<Base> listIterator(int index) { return new DownCastListIterator<Base, Sub>(source.listIterator(index)); }
		public List<Base> subList(int fromIndex, int toIndex) { throw new UnsupportedOperationException(); }
	}

	public static <Base, Sub extends Base> Collection<Base> downCast(Collection<Sub> source) {
		return new DownCastCollection<Base, Sub>(source);
	}
	
	static class DownCastIterator<Base, Sub extends Base> implements Iterator<Base> {
		Iterator<Sub> source;
		DownCastIterator(Iterator<Sub> source) {
			this.source = source;
		}
		public boolean hasNext() { return source.hasNext(); }
		public Base next() { return source.next(); }
	}
	
	static class DownCastCollection<Base, Sub extends Base> implements Collection<Base> {
		Collection<Sub> source;
		DownCastCollection(Collection<Sub> source) {
			this.source = source;
		}
		public int size() { return source.size(); }
		public boolean isEmpty() { return source.isEmpty(); }
		public boolean contains(Object o) { return source.contains(o); }
		public Iterator<Base> iterator() { return new DownCastIterator<Base, Sub>(source.iterator()); }
		public Object[] toArray() { return source.toArray(); }
		public <E> E[] toArray(E[] a) { return source.toArray(a); }
		public boolean add(Base e) { throw new UnsupportedOperationException(); }
		public boolean remove(Object o) { throw new UnsupportedOperationException(); }
		public boolean containsAll(Collection<?> c) { return source.containsAll(c); }
		public boolean addAll(Collection<? extends Base> c) { throw new UnsupportedOperationException(); }
		public boolean removeAll(Collection<?> c) { throw new UnsupportedOperationException(); }
		public boolean retainAll(Collection<?> c) { throw new UnsupportedOperationException(); }
		public void clear() { throw new UnsupportedOperationException(); }
	}
}
