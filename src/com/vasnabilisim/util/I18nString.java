package com.vasnabilisim.util;

import java.util.TreeMap;

/**
 * @author Menderes Fatih GUVEN
 */
public class I18nString {

	private String defaultValue = null;
	private TreeMap<String, String> valueMap = new TreeMap<>();

	public I18nString() {
	}

	public I18nString(String defaultValue) {
	}

	public String getValue() {
		return defaultValue;
	}
	
	public void setValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	public String getValue(String lang) {
		String value = valueMap.get(lang);
		return value == null ? defaultValue : value;
	}
	
	public String setValue(String lang, String value) {
		return valueMap.put(lang, value);
	}
}
