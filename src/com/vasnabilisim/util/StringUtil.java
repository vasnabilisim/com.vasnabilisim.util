package com.vasnabilisim.util;

/**
 * String util class. 
 * Some useful string operations implemented.
 * @author Menderes Fatih GUVEN
 */
public class StringUtil {

	/**
	 * Concats given array of objects as string. 
	 * Null objects omitted. 
	 * Given separator is used between each concatenation.
	 * @param separator
	 * @param args
	 * @return
	 */
	public static String concat(Object separator, Object... args) {
		if(args == null || args.length == 0)
			return "";
		
		StringBuilder result = new StringBuilder(30 * args.length);
		for(Object arg : args) {
			if(arg == null)
				continue;
			if(result.length() > 0 && separator != null)
				result.append(separator);
			result.append(arg);
		}
		return result.toString();
	}

	/**
	 * Concats given array of objects as string. 
	 * Null objects omitted. 
	 * Blank character used as separator. 
	 * A dot put at the end of the string.
	 * @param seperator
	 * @param args
	 * @return
	 */
	public static String sentence(Object... args) {
		String sentence = concat(" ", args);
		return sentence + ".";
	}

	/**
	 * Returns a new string cleared from white spaces.
	 * @param source
	 * @return
	 */
	public static String eraseWhiteSpace(String source) {
		if(source == null)
			return null;
		char[] chars = source.toCharArray();
		char[] result = new char[chars.length];
		int index = 0;
		for(char ch : chars) {
			if(Character.isWhitespace(ch))
				continue;
			result[index++] = ch;
		}
		return new String(result, 0, index);
	}

	public static String fit(String source, int length) {
		return fit(source, length, ' ');
	}

	public static String fit(String source, int length, char empty) {
		if(length <= 0)
			throw new IllegalArgumentException("length can not be non positive.");
		char[] chars = new char[length];
		if(source == null) {
			for(int index=0; index<length; ++index)
				chars[index] = empty;
			return new String(chars);
		}
		if(source.length() == length)
			return source;
		if(source.length() > length)
			return source.substring(0, length-1);
		System.arraycopy(source.toCharArray(), 0, chars, 0, source.length());
		for(int index=source.length(); index<length; ++index)
			chars[index] = empty;
		return new String(chars);
	}

	public static boolean isEmpty(CharSequence s) {
		return s == null || s.length() == 0;
	}
	
	public static String[] toString(Object... objects) {
		String[] result = new String[objects.length];
		for(int index=0; index<objects.length; ++index)
			result[index] = objects[index] == null ? null : String.valueOf(objects[index]);
		return result;
	}
}
