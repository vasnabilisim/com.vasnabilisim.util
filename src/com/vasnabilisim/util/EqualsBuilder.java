package com.vasnabilisim.util;

/**
 * EqualsBuilder
 * @author Menderes Fatih GUVEN
 */
public class EqualsBuilder {

	private boolean result = true;
	
	/**
	 * Checks if given values are equal. 
	 * Both null values considered equal.
	 * Appends to results. 
	 * @param o1
	 * @param o2
	 * @return
	 */
	public <T> EqualsBuilder append(T o1, T o2) {
		if(result)
			result = o1 == null ? (o2 == null) : (o2 != null && o1.equals(o2));
		return this;
	}
	
	/**
	 * Checks if given values are equal. 
	 * Both null values considered not equal.
	 * Appends to results.
	 * @param o1
	 * @param o2
	 * @return
	 */
	public <T> EqualsBuilder appendNotNull(T o1, T o2) {
		if(result)
			result = o1 != null && o2 != null && o1.equals(o2);
		return this;
	}
	
	public EqualsBuilder append(boolean o1, boolean o2) {
		if(result)
			result = o1 == o2;
		return this;
	}
	
	public EqualsBuilder append(byte o1, byte o2) {
		if(result)
			result = o1 == o2;
		return this;
	}
	
	public EqualsBuilder append(char o1, char o2) {
		if(result)
			result = o1 == o2;
		return this;
	}
	
	public EqualsBuilder append(int o1, int o2) {
		if(result)
			result = o1 == o2;
		return this;
	}
	
	public EqualsBuilder append(long o1, long o2) {
		if(result)
			result = o1 == o2;
		return this;
	}
	
	public EqualsBuilder append(float o1, float o2) {
		if(result)
			result = o1 == o2;
		return this;
	}
	
	public EqualsBuilder append(double o1, double o2) {
		if(result)
			result = o1 == o2;
		return this;
	}

	/**
	 * Returns the current result.
	 * @return
	 */
	public boolean toBoolean() {
		return result;
	}
}
