package com.vasnabilisim.util;

/**
 * @author Menderes Fatih GUVEN
 */
public interface KeyValueAppender<K, V> {

	KeyValueAppender<K, V> append(K key, V value);
	
	default KeyValueAppender<K, V> self() {
		return this;
	}
	
	default KeyValueAppender<K, V> append(Pair<K, V> pair) {
		append(pair.getFirst(), pair.getSecond());
		return this;
	}
}
