package com.vasnabilisim.util;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Menderes Fatih GUVEN
 */
public class TreeNode<T> implements Iterable<TreeNode<T>>, Serializable {
	private static final long serialVersionUID = -5261058061585114340L;

	T data;
	int depth = 0;
	
	TreeNode<T> parent;
	
	//siblings share same parent.
	TreeNode<T> prevSibling;
	TreeNode<T> nextSibling;

	//all children are siblings to each other
	TreeNode<T> headChild;
	TreeNode<T> tailChild;
	int childCount = 0;

	public TreeNode() {
	}
	
	public TreeNode(T data) {
		this.data = data;
	}
	
	public T getData() {
		return data;
	}
	
	public void setData(T data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return data == null ? "" : data.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this == obj)
			return true;
		if(!(obj instanceof TreeNode))
			return false;
		TreeNode<?> other = (TreeNode<?>) obj;
		if(this.data == null || other.data == null)
			return false;
		return this.data.equals(other.data);
	}

	public TreeNode<T> getParent() {
		return parent;
	}
	
	public TreeNode<T> getPrevSibling() {
		return prevSibling;
	}
	
	public TreeNode<T> getNextSibling() {
		return nextSibling;
	}

	public TreeNode<T> getHeadChild() {
		return headChild;
	}
	
	public TreeNode<T> getTailChild() {
		return tailChild;
	}
	
	public int getChildCount() {
		return childCount;
	}
	
	public TreeNode<T> getChild(int index) {
		TreeNode<T> child = headChild;
		int i = index;
		while(child != null && i > 0) {
			child = child.nextSibling;
		}
		if(child == null)
			throw new IndexOutOfBoundsException("" + index);
		return child;
	}

	public int indexOfChild(TreeNode<?> find) {
		int index = 0;
		TreeNode<T> child = headChild;
		while(child != null) {
			if(child.equals(find))
				return index;
			++index;
			child = child.nextSibling;
		}
		return -1;
	}
	
	public void addChild(TreeNode<T> child) {
		addChildToTail(child);
	}
	
	public void addChildToTail(TreeNode<T> child) {
		child.remove();
		if(tailChild == null) {
			headChild = tailChild = child;
		} else {
			tailChild.nextSibling = child;
			child.prevSibling = tailChild;
			tailChild = child;
		}
		child.parent = this;
		child.depth = depth + 1;
		++childCount;
	}
	
	public void addChildToHead(TreeNode<T> child) {
		child.remove();
		if(tailChild == null) {
			headChild = tailChild = child;
		} else {
			headChild.prevSibling = child;
			child.nextSibling = headChild;
			headChild = child;
		}
		child.parent = this;
		child.depth = depth + 1;
		++childCount;
	}

	public void addSibling(TreeNode<T> sibling) {
		addSiblingToNext(sibling);
	}
	
	public void addSiblingToPrev(TreeNode<T> sibling) {
		sibling.remove();
		if(prevSibling == null) {
			//this is head
			sibling.nextSibling = this;
			prevSibling = sibling;
			parent.headChild = sibling;
		} else {
			prevSibling.nextSibling = sibling;
			sibling.prevSibling = prevSibling;
			prevSibling = sibling;
			sibling.nextSibling = this;
		}
		sibling.parent = parent;
		sibling.depth = depth;
		++parent.childCount;
	}
	
	public void addSiblingToNext(TreeNode<T> sibling) {
		sibling.remove();
		if(nextSibling == null) {
			//this is tail
			sibling.prevSibling = this;
			nextSibling = sibling;
			parent.tailChild = sibling;
		} else {
			nextSibling.prevSibling = sibling;
			sibling.nextSibling = nextSibling;
			this.nextSibling = sibling;
			sibling.prevSibling = this;
		}
		sibling.parent = parent;
		sibling.depth = depth;
		++parent.childCount;
	}
	
	public void remove() {
		if (parent == null)
			return;
		if (prevSibling == null) {
			if (nextSibling == null) {
				// this is only child to parent
				parent.headChild = parent.tailChild = null;
			} else {
				// this is head
				parent.headChild = nextSibling;
				nextSibling.prevSibling = null;
				nextSibling = null;
			}
		} else {
			if (nextSibling == null) {
				// this is tail
				parent.tailChild = prevSibling;
				prevSibling.nextSibling = null;
				prevSibling = null;
			} else {
				prevSibling.nextSibling = nextSibling;
				nextSibling.prevSibling = prevSibling;
				prevSibling = null;
				nextSibling = null;
			}
		}
		--parent.childCount;
		parent = null;
	}
	
	public void removeAllChildren() {
		while(headChild != null)
			headChild.remove();
		childCount = 0;
	}
	
	public void removeAllSibling() {
		parent.removeAllChildren();
	}
	
	@SuppressWarnings("unchecked")
	public T[] getPath() {
		Object[] path = new Object[depth + 1];
		TreeNode<T> node = this;
		int index = depth;
		while(node != null) {
			path[index--] = node.data;
			node = node.parent;
		}
		return (T[]) path;
	}
	
	@Override
	public Iterator<TreeNode<T>> iterator() {
		return new ChildIterator(this);
	}
	
	public Iterator<TreeNode<T>> childForwardIterator() {
		return new ChildIterator(this);
	}
	
	class ChildIterator implements Iterator<TreeNode<T>> {

		TreeNode<T> parent;
		TreeNode<T> next;
		TreeNode<T> last;
		
		ChildIterator(TreeNode<T> parent) {
			this.parent = parent;
			this.next = parent.headChild;
		}
		
		@Override
		public boolean hasNext() {
			return next != null;
		}

		@Override
		public TreeNode<T> next() {
			if(next == null)
				throw new NoSuchElementException("Illegal navigation.");
			last = next;
			next = next.nextSibling;
			return last;
		}

		@Override
		public void remove() {
			if(last == null)
				throw new IllegalStateException("Illegal navigation.");
			last.remove();
			last = null;
		}
	}
}
