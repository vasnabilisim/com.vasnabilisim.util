package com.vasnabilisim.util;

import java.io.Serializable;
import java.util.List;

/**
 * CloneableList. 
 * Clones items to. 
 * Only clonable objects can be added to clonable list.
 * @author Menderes Fatih GUVEN
 */
public interface CloneableList<T> extends List<T>, Cloneable<CloneableList<T>>, Serializable {
}
