package com.vasnabilisim.util;

import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * A light and serializable map. 
 * @author Menderes Fatih GUVEN
 */
public class ArrayMap<K extends Serializable, V extends Serializable> implements Map<K, V>, Serializable {
	private static final long serialVersionUID = 1834342013178919221L;

	ArrayList<EntryImp> entryList = null;
    
    /**
     * Internal entry class. 
     * Key, value pairs kept as a single entry.
     */
    class EntryImp implements Entry<K, V>, Serializable {
        private static final long serialVersionUID = 7922568351725588201L;
		K key = null;
        V value = null;
		@Override
		public K getKey() {
			return key;
		}
		@Override
		public V getValue() {
			return value;
		}
		@Override
		public V setValue(V value) {
			V oldValue = this.value;
			this.value = value;
			return oldValue;
		}
    }
    
    /**
     * Default constructor.
     * @param initialCapacity
     */
    public ArrayMap() {
        entryList = new ArrayList<EntryImp>();
    }
    
    /**
     * Initial capacity constructor. 
     * Allocates initial capacity many spaces.
     * @param initialCapacity int
     */
    public ArrayMap(int initialCapacity) {
        if(initialCapacity < 0)
            throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
        entryList = new ArrayList<EntryImp>(initialCapacity);
    }
    
    /**
     * @see java.util.Map#size()
     */
    public int size() {
        return entryList.size();
    }
    
    /**
     * @see java.util.Map#isEmpty()
     */
    public boolean isEmpty() {
    	return entryList.isEmpty();
    }
    
    /**
     * @see java.util.Map#containsKey(java.lang.Object)
     */
    public boolean containsKey(Object key) {
    	for(EntryImp entry : entryList) {
    		if(entry.key.equals(key)) 
    			return true;
    	}
        return false;
    }
    
    /**
     * @see java.util.Map#containsValue(java.lang.Object)
     */
    public boolean containsValue(Object value) {
    	for(EntryImp entry : entryList) {
    		if(entry.value.equals(value)) 
    			return true;
    	}
        return false;
    }
    
    /**
     * @see java.util.Map#get(java.lang.Object)
     */
    public V get(Object key) {
        if(key == null)
            return null;
    	for(EntryImp entry : entryList) {
    		if(entry.key.equals(key)) 
    			return entry.value;
    	}
        return null;
    }
    
    /**
     * Returns the key at given index.
     * @param index
     * @return
     */
    public K getKeyAt(int index) {
    	return entryList.get(index).key;
    }
    
    /**
     * Returns the value at given index.
     * @param index
     * @return
     */
    public V getValueAt(int index) {
    	return entryList.get(index).value;
    }
    
    /**
     * @see java.util.Map#put(java.lang.Object, java.lang.Object)
     */
    public V put(K key, V value) {
        if(key == null || value == null)
            return null;
        //If exists update the entity.
    	for(EntryImp entry : entryList) {
    		if(entry.key.equals(key)) {
    			V oldValue = entry.value;
    			entry.value = value;
    			return oldValue;
    		}
    	}
        //Create new entity.
    	EntryImp entry = createEntry(key, value);
    	entryList.add(entry);
        return null;
    }

    /**
     * @see java.util.Map#remove(java.lang.Object)
     */
    public V remove(Object key) {
    	for(EntryImp entry : entryList) {
    		if(entry.key.equals(key)) {
    			entryList.remove(entry);
    			return entry.value;
    		}
    	}
    	return null;
    }

    /**
     * @see java.util.Map#putAll(java.util.Map)
     */
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
            put(e.getKey(), e.getValue());
    }

    /**
     * @see java.util.Map#clear()
     */
    public void clear() {
    	entryList.clear();
    }

    /**
     * @see java.util.Map#keySet()
     */
    public Set<K> keySet() {
	    Set<K> keySet = new AbstractSet<K>() {
            @Override
			public Iterator<K> iterator() {
                return new Iterator<K>() {
                    private int current = 0;
                    public boolean hasNext() {
                        if(current < ArrayMap.this.entryList.size())
                            return true;
                        return false;
                    }
                    public K next() {
                        if(current >= ArrayMap.this.entryList.size())
                            return null;
                        return ArrayMap.this.entryList.get(current++).key;
                    }
                    public void remove() {
                    	throw new UnsupportedOperationException("remove() is not supported.");
                    }
                };//return new Iterator
            }//public Iterator iterator()
            @Override
			public int size() {
                return ArrayMap.this.size();
            }
            @Override
			public boolean contains(Object key) {
                return ArrayMap.this.containsKey(key);
            }
	    };//Set keySet = new AbstractSet()
        
        return keySet;
    }//public Set keySet()

    /**
     * @see java.util.Map#values()
     */
    public Collection<V> values() {
	    Collection<V> values = new AbstractCollection<V>() {
            @Override
			public Iterator<V> iterator() {
                return new Iterator<V>() {
                    private int current = 0;
                    public boolean hasNext() {
                        if(current < ArrayMap.this.entryList.size())
                            return true;
                        return false;
                    }
                    public V next() {
                        if(current >= ArrayMap.this.entryList.size())
                            return null;
                        return ArrayMap.this.entryList.get(current++).value;
                    }
                    public void remove() {
                    	throw new UnsupportedOperationException("remove() is not supported.");
                    }
                };//return new Iterator()
            }//public Iterator iterator()
            @Override
			public int size() {
                return ArrayMap.this.size();
            }
            @Override
			public boolean contains(Object value) {
                return ArrayMap.this.containsValue(value);
            }
	    };//Collection values = new AbstractCollection()

        return values;
    }//public Collection values()

    /**
     * @see java.util.Map#entrySet()
     */
    @Override
    public Set<Entry<K, V>> entrySet() {
    	Set<Entry<K, V>> entrySet = new AbstractSet<Entry<K,V>>() {
			@Override
			public Iterator<Entry<K, V>> iterator() {
				return new Iterator<Entry<K,V>>() {
					private int current = 0;
					public boolean hasNext() {
                        if(current < ArrayMap.this.entryList.size())
                            return true;
                        return false;
					}
					public Entry<K, V> next() {
                        if(current >= ArrayMap.this.entryList.size())
                            return null;
                        return ArrayMap.this.entryList.get(current++);
					}
					public void remove() {
                    	throw new UnsupportedOperationException("remove() is not supported.");
					}
				};
			}
			@Override
			public int size() {
				return ArrayMap.this.entryList.size();
			}
			@Override
			public boolean contains(Object o) {
				return ArrayMap.this.entryList.contains(o);	
			}
    	};
    	return entrySet;	
    }
    
    /**
     * Creates a new entry.
     * @param key
     * @param value
     * @return
     */
    private EntryImp createEntry(K key, V value) {
    	EntryImp entity = new EntryImp();
        entity.key = key;
        entity.value = value;
        return entity;
    }
}
