package com.vasnabilisim.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

/**
 * CloneableLinkedList
 * @author Menderes Fatih GUVEN
 */
public class CloneableLinkedList<T extends Cloneable<T>> extends LinkedList<T> 
implements Serializable, CloneableList<T>, java.lang.Cloneable
{
	private static final long serialVersionUID = -6261328384216637352L;

	/**
	 * Default constructor.
	 */
	public CloneableLinkedList() {
		super();
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	public CloneableLinkedList(Collection<T> source) {
		super();
		for(T item : source)
			this.add(item.cloneObject());
	}

	/**
	 * @see java.util.LinkedList#clone()
	 */
	@Override
	public Object clone() {
		return cloneObject();
	}

	/**
	 * @see com.vasnabilisim.util.Cloneable#cloneObject()
	 */
	@Override
	public CloneableLinkedList<T> cloneObject() {
		return new CloneableLinkedList<T>(this);
	}
}
