package com.vasnabilisim.util;

/**
 * Generic Clonable interface.
 * @author Menderes Fatih GUVEN
 */
public interface Cloneable<T> {

	/**
	 * Clones the object.
	 * @return
	 */
	T cloneObject();
}
