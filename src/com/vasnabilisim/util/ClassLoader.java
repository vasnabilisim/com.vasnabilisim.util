package com.vasnabilisim.util;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import com.vasnabilisim.io.JarFileFilter;

/**
 * ClassLoader
 * @author Menderes Fatih GUVEN
 */
public class ClassLoader {

	private static java.lang.ClassLoader classLoader = java.lang.ClassLoader.getSystemClassLoader();
	
	public static void init(String libPathName) throws IOException {
		File libPath = new File(libPathName);
		if (!libPath.exists())
			return;
		
		File[] jarFiles = libPath.listFiles((FileFilter)new JarFileFilter());
		ArrayList<URL> jarURLList = new ArrayList<URL>(jarFiles.length);
		for (int index = 0; index < jarFiles.length; ++index) {
			try {
				jarURLList.add(new URL("file:" + jarFiles[index].getPath()));
			} catch (MalformedURLException e) {
				throw new IOException(String.format("Unable to load lib [%s]", jarFiles[index].getPath()), e);
			}
		}
		URLClassLoader classLoader = new URLClassLoader(
				jarURLList.toArray(new URL[jarURLList.size()]), 
				java.lang.ClassLoader.getSystemClassLoader()
		);

		ClassLoader.setClassLoader(classLoader);
		Thread.currentThread().setContextClassLoader(classLoader);
	}
	
	public static java.lang.ClassLoader getClassLoader() {
		return classLoader;
	}
	
	public static void setClassLoader(java.lang.ClassLoader classLoader) {
		ClassLoader.classLoader = classLoader;
	}
	
	public static Class<?> loadClass(String name) throws ClassNotFoundException {
		return getClassLoader().loadClass(name);
	}

	@SuppressWarnings("unchecked")
	public static <T> Class<? extends T> loadClass(String name, Class<T> type) throws ClassNotFoundException, ClassCastException {
		Class<?> clazz = getClassLoader().loadClass(name);
		if(type.isAssignableFrom(clazz))
			return (Class<? extends T>)clazz;
		throw new ClassCastException();
	}
}
