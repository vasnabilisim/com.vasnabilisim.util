package com.vasnabilisim.util.item;

import java.util.Collection;

/**
 * @author Menderes Fatih GUVEN
 */
public class DefaultItemList<E> extends ItemList<E, DefaultItem<E>> {

	public DefaultItemList(Collection<E> values) {
		super(new DefaultItemFactory<>(), values);
	}

	@SafeVarargs
	public DefaultItemList(E... values) {
		super(new DefaultItemFactory<>(), values);
	}
}
