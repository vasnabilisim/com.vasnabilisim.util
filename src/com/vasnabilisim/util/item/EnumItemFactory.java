package com.vasnabilisim.util.item;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author Menderes Fatih GUVEN
 */
public class EnumItemFactory<E extends Enum<E>> implements ItemFactory<E, EnumItem<E>> {

	Class<E> enumType;
	TreeMap<E, String> enumTextMap = new TreeMap<>();
	//EnumDataList<E> enumDataList;
	
	public EnumItemFactory(Class<E> enumType) {
		this.enumType = enumType;
		
		E[] enumLiterals = enumType.getEnumConstants();

		Properties properties = new Properties();
		
		InputStream in = enumType.getResourceAsStream(enumType.getSimpleName() + ".resource");
		InputStreamReader reader = null;
		try {
			reader = new InputStreamReader(in, "UTF-8");
			properties.load(reader);
		} catch (IOException e) {
			if(reader != null)
				try { reader.close(); } catch (IOException ex) { }
			else if (in != null)
				try { in.close(); } catch (IOException ex) {}
		}

		for(E enumLiteral : enumLiterals) {
			String text = properties.getProperty(enumLiteral.name());
			enumTextMap.put(enumLiteral, text == null ? enumLiteral.name() : text);
		}
	}
	
	public Set<EnumItem<E>> asSet() {
		Set<EnumItem<E>> set = new HashSet<>(enumTextMap.size());
		for(Map.Entry<E, String> entry : enumTextMap.entrySet())
			set.add(new EnumItem<>(entry.getKey(), entry.getValue()));
		return set;
	}
	
	public List<EnumItem<E>> asList() {
		List<EnumItem<E>> list = new ArrayList<>(enumTextMap.size());
		for(Map.Entry<E, String> entry : enumTextMap.entrySet())
			list.add(new EnumItem<>(entry.getKey(), entry.getValue()));
		return list;
	}

	@Override
	public EnumItem<E> create(E value) {
		if(value == null)
			return new EnumItem<>(null, null);
		return new EnumItem<>(value, enumTextMap.get(value));
	}

}
