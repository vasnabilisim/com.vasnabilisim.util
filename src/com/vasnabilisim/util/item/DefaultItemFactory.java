package com.vasnabilisim.util.item;

/**
 * @author Menderes Fatih GUVEN
 */
public class DefaultItemFactory<E> implements ItemFactory<E, DefaultItem<E>> {

	public DefaultItemFactory() {
	}

	@Override
	public DefaultItem<E> create(E value) {
		return new DefaultItem<>(value);
	}

}
