package com.vasnabilisim.util.item;

/**
 * @author Menderes Fatih GUVEN
 */
public class EnumItem<E extends Enum<E>> implements ComparableItem<E> {

	E enumLiteral = null;
	String text = null;
	
	EnumItem(E enumLiteral, String text) {
		this.enumLiteral = enumLiteral;
		this.text = text == null ? "" : text;
	}
	
	@Override
	public E value() {
		return enumLiteral;
	}
	
	public String getText() {
		return text;
	}
	
	@Override
	public String toString() {
		return text;
	}
}
