package com.vasnabilisim.util.item;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Menderes Fatih GUVEN
 */
public class ItemList<E, I extends Item<E>> extends AbstractList<I> implements ItemCollection<E, I> {

	ArrayList<I> source;
	
	public ItemList(ItemFactory<E, I> factory, Collection<E> values) {
		source = new ArrayList<>(values.size());
		for(E e : values)
			source.add(factory.create(e));
	}
	
	@SafeVarargs
	public ItemList(ItemFactory<E, I> factory, E... values) {
		source = new ArrayList<>(values.length);
		for(E e : values)
			source.add(factory.create(e));
	}

	@Override
	public I get(int index) {
		return source.get(index);
	}

	@Override
	public int size() {
		return source.size();
	}

}
