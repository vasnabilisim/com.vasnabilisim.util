package com.vasnabilisim.util.item;

/**
 * @author Menderes Fatih GÜVEN
 */
public class DefaultComparableItem<C extends Comparable<C>> extends DefaultItem<C> implements ComparableItem<C> {

	DefaultComparableItem(C value) {
		super(value);
	}
}
