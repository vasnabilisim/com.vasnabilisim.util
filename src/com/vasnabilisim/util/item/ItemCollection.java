package com.vasnabilisim.util.item;

import java.util.Collection;

/**
 * @author Menderes Fatih GUVEN
 */
public interface ItemCollection<E, I extends Item<E>> extends Collection<I> {
}
