package com.vasnabilisim.util.item;

/**
 * @author Menderes Fatih GUVEN
 */
public interface ComparableItem<C extends Comparable<C>> extends Item<C>, Comparable<ComparableItem<C>> {

	@Override
	default int compareTo(ComparableItem<C> other) {
		if(other == null)
			return 1;
		if(this == other)
			return 0;
		C thisValue = this.value();
		C otherValue = other.value();
		if(thisValue == null) {
			if(otherValue == null)
				return 0;
			return -1;
		}
		if(otherValue == null)
			return 1;
		return thisValue.compareTo(otherValue);
	}
}
