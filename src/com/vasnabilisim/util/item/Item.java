package com.vasnabilisim.util.item;

/**
 * @author Menderes Fatih GUVEN
 */
public interface Item<E> {

	E value();
}
