package com.vasnabilisim.util.item;

/**
 * @author Menderes Fatih GUVEN
 */
public class DefaultItem<E> implements Item<E> {

	E value;
	
	DefaultItem(E value) {
		this.value = value;
	}
	
	@Override
	public E value() {
		return value;
	}
	
	@Override
	public String toString() {
		return value == null ? "" : value.toString();
	}
}
