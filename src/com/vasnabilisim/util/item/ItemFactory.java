package com.vasnabilisim.util.item;

/**
 * @author Menderes Fatih GUVEN
 */
public interface ItemFactory<E, I extends Item<E>> {

	I create(E value);
}
