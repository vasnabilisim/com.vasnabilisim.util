package com.vasnabilisim.util.item;

/**
 * @author Menderes Fatih GUVEN
 */
public class EnumItemList<E extends Enum<E>> extends ItemList<E, EnumItem<E>> {

	Class<E> enumType;
	
	public EnumItemList(Class<E> enumType) {
		super(new EnumItemFactory<>(enumType), enumType.getEnumConstants());
		this.enumType = enumType;
	}
	
	public Class<E> getEnumType() {
		return enumType;
	}
}
