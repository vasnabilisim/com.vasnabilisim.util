package com.vasnabilisim.util;

import java.util.Iterator;

/**
 * Iterator Iterative. 
 * @author Menderes Fatih GUVEN
 */
public class IterableIterative<T> implements Iterative<T> {

	/**
	 * Iterable constructor.
	 * @param c
	 */
	public IterableIterative(Iterable<T> c) {
		this.iterable = c;
	}
	
	private Iterable<T> iterable = null;

	private Iterator<T> iterator = null;
	private T current = null;

	/**
	 * @see com.vasnabilisim.util.Iterative#start()
	 */
	@Override
	public void start() {
		iterator = iterable.iterator();
		current = null;
	}

	/**
	 * @see com.vasnabilisim.util.Iterative#stop()
	 */
	@Override
	public void stop() {
		iterator = null;
		current = null;
	}

	/**
	 * @see com.vasnabilisim.util.Iterative#current()
	 */
	@Override
	public T current() {
		if(iterator == null)
			throw new IllegalStateException("Iterative not started.");
		return current;
	}

	/**
	 * @see com.vasnabilisim.util.Iterative#hasNext()
	 */
	@Override
	public boolean hasNext() {
		if(iterator == null)
			throw new IllegalStateException("Iterative not started.");
		return iterator.hasNext();
	}

	/**
	 * @see com.vasnabilisim.util.Iterative#next()
	 */
	@Override
	public T next() {
		if(iterator == null)
			throw new IllegalStateException("Iterative not started.");
		current = iterator.next();
		return current;
	}
}
