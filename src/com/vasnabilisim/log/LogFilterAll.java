package com.vasnabilisim.log;

import java.util.logging.Filter;
import java.util.logging.LogRecord;

/**
 * No filter all records are logged.
 * 
 * @author Menderes Fatih GUVEN
 */
public class LogFilterAll implements Filter {

	/**
	 * @see java.util.logging.Filter#isLoggable(java.util.logging.LogRecord)
	 */
	public boolean isLoggable(LogRecord record) {
		return true;
	}
}
