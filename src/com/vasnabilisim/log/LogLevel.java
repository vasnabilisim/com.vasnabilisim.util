package com.vasnabilisim.log;

import java.util.logging.Level;

/**
 * Logging level class.
 * 
 * @author Menderes Fatih GUVEN
 */
public class LogLevel extends Level {
	private static final long serialVersionUID = 7171741057988765265L;

	/**
	 * QuerySingle constructor.
	 * 
	 * @param key
	 * @param model
	 */
	public LogLevel(String name, int value) {
		super(name, value);
	}

	/**
	 * Trace log level. Mostly used for debugging reasons.
	 */
	public static final LogLevel TRACE = new LogLevel("T", 400);
	/**
	 * Info log level.
	 */
	public static final LogLevel INFO = new LogLevel("I", 800);
	/**
	 * Warning log level.
	 */
	public static final LogLevel WARNING = new LogLevel("W", 900);
	/**
	 * Error log level.
	 */
	public static final LogLevel ERROR = new LogLevel("E", 1000);
	/**
	 * Fatal error log level.
	 */
	public static final LogLevel FATAL = new LogLevel("F", 1100);
}
