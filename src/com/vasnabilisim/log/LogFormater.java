package com.vasnabilisim.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Log formater class. Formats log records.
 * 
 * @author Menderes Fatih GUVEN
 */
public class LogFormater extends Formatter {
	private boolean handleStackTrace = false;
	private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
	private String lineSeperator = "\r\n";

	/**
	 * Simple constructor. If given argument is true stack trace of log records
	 * also formated to be logged.
	 * 
	 * @param handleStackTrace
	 */
	public LogFormater(boolean handleStackTrace) {
		this.handleStackTrace = handleStackTrace;
	}

	/**
	 * Formats the given log record.
	 * 
	 * @param record
	 * @return
	 */
	@Override
	public synchronized String format(LogRecord record) {
		if (record == null)
			return "";

		StringBuilder builder = new StringBuilder(200);
		builder.append(dateTimeFormatter.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(record.getMillis()), ZoneId.systemDefault())));
		builder.append(" ");
		builder.append(record.getParameters()[0]);
		builder.append(" ");
		builder.append(record.getSourceClassName());
		builder.append(" ");
		builder.append(record.getSourceMethodName());
		builder.append(lineSeperator);

		String message = record.getMessage();
		Throwable e = record.getThrown();
		if (message == null && e != null)
			message = e.getMessage();
		builder.append(record.getLevel().getName());
		builder.append(" ");
		builder.append(message);
		builder.append(lineSeperator);
		if (handleStackTrace) {
			if (e != null) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				pw.close();
				builder.append(sw.toString());
			}
		}// if(handleStackTrace)

		return builder.toString();
	}// format
}
