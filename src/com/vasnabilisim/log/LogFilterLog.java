package com.vasnabilisim.log;

import java.util.logging.Filter;
import java.util.logging.LogRecord;

/**
 * Log type log handler filter.
 * 
 * @author Menderes Fatih GUVEN
 */
public class LogFilterLog implements Filter {

	/**
	 * @see java.util.logging.Filter#isLoggable(java.util.logging.LogRecord)
	 */
	public boolean isLoggable(LogRecord record) {
		if (record == null)
			return false;
		return LogLevel.INFO.intValue() <= record.getLevel().intValue();
	}
}
