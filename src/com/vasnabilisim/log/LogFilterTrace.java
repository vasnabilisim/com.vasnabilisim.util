package com.vasnabilisim.log;

import java.util.logging.Filter;
import java.util.logging.LogRecord;

/**
 * Trace type log handler filter.
 * 
 * @author Menderes Fatih GUVEN
 */
public class LogFilterTrace implements Filter {

	/**
	 * @see java.util.logging.Filter#isLoggable(java.util.logging.LogRecord)
	 */
	public boolean isLoggable(LogRecord record) {
		if (record == null)
			return false;
		return LogLevel.TRACE.equals(record.getLevel());
	}
}
