package com.vasnabilisim.xmlmodel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.TreeMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.vasnabilisim.dataconverter.DataConverter;
import com.vasnabilisim.util.StringUtil;
import com.vasnabilisim.xml.XmlException;

public class DefaultXmlDecoder implements XmlDecoder {

	public DefaultXmlDecoder() {
	}

	protected TreeMap<String, Object> refMap = new TreeMap<>();
	
	@Override
	public Object putRef(String name, Object ref) {
		return refMap.put(name, ref);
	}

	@Override
	public Object decode(Document document, XmlModel model) throws XmlException {
		XmlModelObject rootModelObject = model.getRoot();
		Element rootElement = document.getDocumentElement();
		if (!rootModelObject.getXmlName().equals(rootElement.getNodeName()))
			throw new XmlException(String.format("Invalid xml document. Expecting %s found %s.", rootModelObject.getXmlName(), rootElement.getNodeName()));
		Object root;
		try {
			root = rootModelObject.getInstanceType().newInstance();
		} catch (Exception e) {
			// InstantiationException
			// IllegalAccessException
			String message = String.format("Unable to instantiate %s.", rootModelObject.getInstanceType().getName());
			throw new XmlException(message, e);
		}
		fromDocumentToObject(root, rootElement, rootModelObject);
		return root;
	}

	/**
	 * Recursively reads sub elements of given elements and convert them to objects. 
	 * Sets given objects fields from read objects.  
	 * @param object
	 * @param element
	 * @param modelObject
	 * @throws XmlException
	 */
	protected void fromDocumentToObject(
			Object object, 
			Element element, 
			XmlModelObject xmlModelObject) throws XmlException 
	{
		for (Node node = element.getFirstChild(); node != null; node = node.getNextSibling()) {
			if (node.getNodeType() != Node.ELEMENT_NODE)
				continue;
			String xmlName = node.getNodeName();
			XmlModelNode xmlModelNode = xmlModelObject.findNode(xmlName);
			if(xmlModelNode == null)
				continue;
			if(xmlModelNode instanceof XmlModelProperty)
				fromDocumentToObject_SetProperty(object, (Element)node, (XmlModelProperty)xmlModelNode);
			else /*if(xmlModelNode instanceof XmlModelObject)*/ {
				fromDocumentToObject_SetObject(object, (Element)node, (XmlModelObject)xmlModelNode);
			}
		}
	}

	/**
	 * Sets parentObject s field identified by given xmlModelObject. 
	 * Object read from given element. 
	 * @param parentObject
	 * @param element
	 * @param xmlModelObject
	 * @throws XmlException
	 */
	protected void fromDocumentToObject_SetObject(Object parentObject, Element element, XmlModelObject xmlModelObject) throws XmlException {
		if(element.hasAttribute("ref")) {
			String refName = element.getAttribute("ref");
			if(StringUtil.isEmpty(refName))
				throw new XmlException("Empty ref name");
			Object ref = refMap.get(refName);
			if(ref == null) {
				String message = String.format("Unable to find reference object named %s.", refName);
				throw new XmlException(message);
			}
			fromDocumentToObject_SetField(parentObject, ref, xmlModelObject);
			return;
		}
		Object object;
		try {
			object = xmlModelObject.getInstanceType().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			String message = String.format("Unable to instantiate %s.", xmlModelObject.getInstanceType().getName());
			throw new XmlException(message, e);
		}
		if(xmlModelObject.getFullConstructBeforeAppend()) {
			fromDocumentToObject(object, element, xmlModelObject);
			fromDocumentToObject_SetField(parentObject, object, xmlModelObject);
		}
		else {
			fromDocumentToObject_SetField(parentObject, object, xmlModelObject);
			fromDocumentToObject(object, element, xmlModelObject);
		}
	}

	/**
	 * Sets parentObject s field identified by given xmlModelProperty. 
	 * Value read from given element. 
	 * Value converted to required object type to met the requirement. 
	 * If not exception thrown. 
	 * @param parentObject
	 * @param element
	 * @param xmlModelProperty
	 * @throws XmlException
	 */
	protected void fromDocumentToObject_SetProperty(Object parentObject, Element element, XmlModelProperty xmlModelProperty) throws XmlException {
		String stringValue = element.getTextContent();
		Object value = null;
		if (xmlModelProperty.getType().isEnum()) {
			Object[] enumConstants = xmlModelProperty.getType().getEnumConstants();
			for (Object enumConstant : enumConstants)
				if (enumConstant.toString().equals(stringValue)) {
					value = enumConstant;
					break;
				}
			if (value == null) {
				String message = String.format("Unidentified value %s for property %s", stringValue, xmlModelProperty.getObjectName());
				throw new XmlException(message);
			} 
		}
		else
			value = DataConverter.convertTo(stringValue, xmlModelProperty.getType());
		fromDocumentToObject_SetField(parentObject, value, xmlModelProperty);
	}
	
	/**
	 * Sets given value to given object s field identified by given xmlModelNode
	 * @param object
	 * @param value
	 * @param xmlModelNode
	 * @throws XmlException
	 */
	protected void fromDocumentToObject_SetField(Object object, Object value, XmlModelNode xmlModelNode) throws XmlException {
		String methodName = null;
		if(xmlModelNode.getMultiplicity() == null || xmlModelNode.getMultiplicity() == Multiplicity.Single)
			methodName = "set" + xmlModelNode.getObjectName();
		else
			methodName = "add" + xmlModelNode.getObjectName();
		Method method = null;
		try {
			method = xmlModelNode.getParent().getInterfaceType().getMethod(methodName, xmlModelNode.getType());
		} catch (NoSuchMethodException | SecurityException e) {
			for(Method methodIte : object.getClass().getMethods()) {
				if(methodIte.getParameterCount() != 1)
					continue;
				if(!methodIte.getName().equals(methodName))
					continue;
				if(!methodIte.getParameterTypes()[0].isAssignableFrom(xmlModelNode.getType()))
					continue;
				method = methodIte;
				break;
			}
			if(method == null) {
				String message = String.format("Unable to find method %s(%s) in class %s.", methodName, xmlModelNode.getType(), object.getClass().getName());
				throw new XmlException(message, e);
			}
		}
		try {
			method.invoke(object, value);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			String message = String.format("Unable to invoke method %s(%s) in class %s.", method.getName(), method.getParameterTypes()[0].getName(), object.getClass().getName());
			throw new XmlException(message, e);
		}
	}
}
