package com.vasnabilisim.xmlmodel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.vasnabilisim.util.Tree;
import com.vasnabilisim.util.TreeNode;
import com.vasnabilisim.xml.XmlException;

/**
 * @author Menderes Fatih GUVEN
 */
public class XmlModelNavigator {

	private XmlModel xmlModel;
	
	public XmlModelNavigator(XmlModel xmlModel) {
		this.xmlModel = xmlModel;
	}
	
	public XmlModel getXmlModel() {
		return xmlModel;
	}
	
	public Tree<Object> toTree(Object root, Set<String> objectNames) throws XmlException {
		Tree<Object> tree = new Tree<Object>();

		XmlModelObject rootXmlModelObject = xmlModel.getRoot();
		TreeNode<Object> rootNode = null;
		if(objectNames.contains(rootXmlModelObject.getObjectName()))
			rootNode = toTreeNode(rootXmlModelObject, root, objectNames);
		else
			rootNode = new TreeNode<Object>("");
		
		tree.setRoot(rootNode);
		return tree;
	}
	
	public TreeNode<Object> toTreeNode(XmlModelObject parentXmlModelObject, Object parent, Set<String> objectNames) throws XmlException {
		TreeNode<Object> parentNode = new TreeNode<>(parent);
		
		for(XmlModelObject childXmlModelObject : parentXmlModelObject.getObjectList()) {
			if(!objectNames.contains(childXmlModelObject.getObjectName()))
				continue;
			String methodName = "get" + childXmlModelObject.getObjectName();
			if(childXmlModelObject.getMultiplicity() == Multiplicity.Single) {
				Object child = invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				TreeNode<Object> childNode = toTreeNode(childXmlModelObject, child, objectNames);
				parentNode.addChildToTail(childNode);
			}		
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.List) {
				methodName += "List";
				List<?> children = (List<?>)invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				for(Object child : children) {
					TreeNode<Object> childNode = toTreeNode(childXmlModelObject, child, objectNames);
					parentNode.addChildToTail(childNode);
				}
			}
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.Set) {
				methodName += "Set";
				Set<?> children = (Set<?>)invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				for(Object child : children) {
					TreeNode<Object> childNode = toTreeNode(childXmlModelObject, child, objectNames);
					parentNode.addChildToTail(childNode);
				}
			}
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.Array) {
				methodName += "s";
				Object[] children = (Object[])invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				for(Object child : children) {
					TreeNode<Object> childNode = toTreeNode(childXmlModelObject, child, objectNames);
					parentNode.addChildToTail(childNode);
				}
			}
		}

		return parentNode;
	}
	
	public int getChildObjectCount(Object root) throws XmlException {
		return getChildObjectCount(xmlModel.getRoot(), root);
	}

	public int getChildObjectCount(XmlModelObject parentXmlModelObject, Object parent) throws XmlException {
		if(!parentXmlModelObject.getInterfaceType().isInstance(parent))
			throw new IllegalArgumentException(String.format("Expecting %s found %s", parentXmlModelObject.getInterfaceType().getName(), parent.getClass().getName()));
		int childObjectCount = 0;
		for(XmlModelObject childXmlModelObject : parentXmlModelObject.getObjectList()) {
			if(childXmlModelObject.getMultiplicity() == Multiplicity.Single) {
				++childObjectCount;
				continue;
			}		
			String methodName = "get" + childXmlModelObject.getObjectName();
			if(childXmlModelObject.getMultiplicity() == Multiplicity.List) {
				methodName += "List";
				List<?> children = (List<?>)invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				childObjectCount += children.size();
			}
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.Set) {
				methodName += "Set";
				Set<?> children = (Set<?>)invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				childObjectCount += children.size();
			}
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.Array) {
				methodName += "s";
				Object[] children = (Object[])invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				childObjectCount += children.length;
			}
		}
		
		return childObjectCount;
	}

	public List<?> getChildObjectList(XmlModelObject parentXmlModelObject, Object parent) throws XmlException {
		if(!parentXmlModelObject.getInterfaceType().isInstance(parent))
			throw new IllegalArgumentException(String.format("Expecting %s found %s", parentXmlModelObject.getInterfaceType().getName(), parent.getClass().getName()));
		List<Object> childList = new ArrayList<>();
		for(XmlModelObject childXmlModelObject : parentXmlModelObject.getObjectList()) {
			String methodName = "get" + childXmlModelObject.getObjectName();
			if(childXmlModelObject.getMultiplicity() == Multiplicity.Single) {
				Object child = invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				childList.add(child);
			}		
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.List) {
				methodName += "List";
				List<?> children = (List<?>)invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				childList.addAll(children);
			}
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.Set) {
				methodName += "Set";
				Set<?> children = (Set<?>)invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				childList.addAll(children);
			}
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.Array) {
				methodName += "s";
				Object[] children = (Object[])invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				for(Object child : children)
					childList.add(child);
			}
		}
		return childList;
	}

	public Object getChildObject(XmlModelObject parentXmlModelObject, Object parent, int pIndex) throws XmlException {
		if(!parentXmlModelObject.getInterfaceType().isInstance(parent))
			throw new IllegalArgumentException(String.format("Expecting %s found %s", parentXmlModelObject.getInterfaceType().getName(), parent.getClass().getName()));
		int index = pIndex;
		for(XmlModelObject childXmlModelObject : parentXmlModelObject.getObjectList()) {
			String methodName = "get" + childXmlModelObject.getObjectName();
			if(childXmlModelObject.getMultiplicity() == Multiplicity.Single) {
				if(index == 0)
					return invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				--index;
			}		
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.List) {
				methodName += "List";
				List<?> children = (List<?>)invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				if(index < children.size())
					return children.get(index);
				index -= children.size();
			}
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.Set) {
				methodName += "Set";
				Set<?> children = (Set<?>)invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				if(index < children.size()) {
					Iterator<?> iterator = children.iterator();
					while(index > 0) {
						iterator.next();
						--index;
					}
					return iterator.next();
				}
				index -= children.size();
			}
			else if(childXmlModelObject.getMultiplicity() == Multiplicity.Array) {
				methodName += "s";
				Object[] children = (Object[])invokeMethod(parentXmlModelObject.getInterfaceType(), methodName, null, parent, null);
				if(index < children.length)
					return children[index];
				index -= children.length;
			}
		}
		throw new IndexOutOfBoundsException("" + pIndex);
	}
	
	private Object invokeMethod(Class<?> clazz, String methodName, Class<?>[] argClasses, Object object, Object[] args) throws XmlException {
		return invokeMethod(findMethod(clazz, methodName, argClasses), object, args);
	}

	private Method findMethod(Class<?> clazz, String methodName, Class<?>... argClasses) throws XmlException {
		try {
			return clazz.getMethod(methodName, argClasses);
		} catch (NoSuchMethodException | SecurityException e) {
			String message = String.format("Unable to find method %s in class %s.", methodName, clazz.getName());
			throw new XmlException(message, e);
		}
	}
	
	private Object invokeMethod(Method method, Object object, Object... args) throws XmlException {
		try {
			return method.invoke(object, args);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			String message = String.format("Unable to invoke method %s in class %s.", method.getName(), method.getDeclaringClass().getName());
			throw new XmlException(message, e);
		}
	}	
}
