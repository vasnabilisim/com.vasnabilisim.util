package com.vasnabilisim.xmlmodel;

import java.io.Serializable;

/**
 * @author Menderes Fatih GUVEN
 */
public class XmlModelProperty extends XmlModelNode implements Serializable {
	private static final long serialVersionUID = -7865072096835224267L;
	
	public XmlModelProperty() {
	}
	
	public XmlModelProperty(String xmlName, Class<?> type) {
		super(xmlName, null, type, Multiplicity.Single);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append("<xml-name=").append(xmlName).append(", ");
		builder.append("object-name=").append(objectName).append(", ");
		builder.append("type=").append(type).append(", ");
		builder.append("multiplicity=").append(multiplicity).append(">");
		return builder.toString();
	}

	public Class<?> getType() {
		return type;
	}
	
	public void setType(Class<?> type) {
		this.type = type;
	}
}
