package com.vasnabilisim.xmlmodel;

/**
 * @author Menderes Fatih GUVEN
 */
public enum Multiplicity {
	
	Single(true), 
	Array(false), 
	Set(false), 
	List(false);

	boolean single;
	
	Multiplicity(boolean single) {
		this.single = single;
	}
	
	public boolean isSingle() {
		return single;
	}
}
