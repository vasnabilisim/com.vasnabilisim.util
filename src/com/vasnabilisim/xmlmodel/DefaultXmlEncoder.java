package com.vasnabilisim.xmlmodel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.dataconverter.DataConverter;
import com.vasnabilisim.xml.XmlException;
import com.vasnabilisim.xml.XmlParser;

/**
 * @author Menderes Fatih GUVEN
 */
public class DefaultXmlEncoder implements XmlEncoder {

	public DefaultXmlEncoder() {
	}

	/**
	 * @see com.vasnabilisim.xmlmodel.XmlEncoder#encode(java.lang.Object, com.vasnabilisim.xmlmodel.XmlModel)
	 */
	@Override
	public Document encode(Object object, XmlModel model) throws XmlException {
		XmlModelObject rootModelElement = model.getRoot();
		if (!rootModelElement.getInterfaceType().isInstance(object))
			throw new XmlException(String.format("Unexpected object. Expecting %s found %s.", rootModelElement.getInterfaceType().getName(), object.getClass().getName()));
		Document document = XmlParser.newDocument();
		Element rootElement = document.createElement(rootModelElement.getXmlName());
		fromObjectToDocument(object, document, rootElement, rootModelElement);
		document.appendChild(rootElement);
		return document;
	}

	protected void fromObjectToDocument(Object parent, Document document,
			Element parentElement, XmlModelObject parentModelElement)
			throws XmlException {
		fromPropertyToDocument(parent, document, parentElement, parentModelElement);

		for (XmlModelObject modelElement : parentModelElement.getObjectList()) {
			String methodName = "get" + modelElement.getObjectName();
			if (modelElement.getMultiplicity() == Multiplicity.List)
				methodName += "List";
			else if (modelElement.getMultiplicity() == Multiplicity.Set)
				methodName += "Set";
			// TODO Array support
			Method method;
			try {
				method = parent.getClass().getMethod(methodName);
			} catch (NoSuchMethodException | SecurityException e) {
				String message = String.format("Unable to find method %s in class %s.", methodName, parent.getClass().getName());
				throw new XmlException(message, e);
			}
			Object object;
			try {
				object = method.invoke(parent);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				String message = String.format("Unable to invoke method %s in class %s.", methodName, parent.getClass().getName());
				throw new XmlException(message, e);
			}
			Element element = document.createElement(modelElement.getXmlName());
			if (modelElement.getMultiplicity() == Multiplicity.Single) {
				fromObjectToDocument(object, document, parentElement, parentModelElement);
			} else if (modelElement.getMultiplicity() == Multiplicity.List) {
				if (!(object instanceof List<?>))
					throw new XmlException(String.format("Unexpected return type for method %s in class %s. Expecting java.util.List found %s.", 
							methodName, 
							parent.getClass().getName(), 
							object.getClass().getName()));
				List<?> list = (List<?>) object;
				for (Object entry : list)
					fromObjectToDocument(entry, document, parentElement, parentModelElement);
			} else if (modelElement.getMultiplicity() == Multiplicity.Set) {
				if (!(object instanceof Set<?>))
					throw new XmlException(String.format("Unexpected return type for method %s in class %s. Expecting java.util.Set found %s.",
							methodName, 
							parent.getClass().getName(), 
							object.getClass().getName()));
				Set<?> set = (Set<?>) object;
				for (Object entry : set)
					fromObjectToDocument(entry, document, parentElement, parentModelElement);
			}
			parentElement.appendChild(element);
		}
	}

	protected void fromPropertyToDocument(Object parent, Document document, Element parentElement, XmlModelObject parentModelElement) throws XmlException {
		for (XmlModelProperty modelProperty : parentModelElement.getPropertyList()) {
			Object propertyValue = null;
			String methodName = "get" + modelProperty.getObjectName();
			Method method;
			try {
				method = parent.getClass().getMethod(methodName);
			} catch (NoSuchMethodException | SecurityException e) {
				String message = String.format("Unable to find method %s in class %s.", methodName, parent.getClass().getName());
				throw new XmlException(message, e);
			}
			if (method == null)
				throw new XmlException(String.format("Unexpected object. Unable to find method %s in class %s.", methodName, parent.getClass().getName()));
			if (!method.getReturnType().equals(modelProperty.getType()))
				throw new XmlException(String.format("Unexpected return type for method %s in class %s. Expecting %s found %s.", 
						methodName, 
						parent.getClass().getName(), 
						modelProperty.getType().getName(), 
						method.getReturnType().getName()));
			try {
				propertyValue = method.invoke(parent);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				String message = String.format("Unable to invoke method %s in class %s.", 
						methodName, parent.getClass().getName());
				throw new XmlException(message, e);
			}
			fromPropertyToDocument(
					document, 
					parentElement, 
					modelProperty.getXmlName(), 
					DataConverter.convertTo(propertyValue, String.class)
			);
		}
	}
	
	protected void fromPropertyToDocument(Document document, Element parentElement, String propertyName, String propertyValue) {
		Element propertyElement = document.createElement(propertyName);
		propertyElement.setTextContent(propertyValue);
		parentElement.appendChild(propertyElement);
	}
}
