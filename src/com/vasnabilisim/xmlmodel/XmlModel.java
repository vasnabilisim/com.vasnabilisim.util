package com.vasnabilisim.xmlmodel;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;

import org.w3c.dom.Document;

import com.vasnabilisim.xml.XmlException;
import com.vasnabilisim.xml.XmlParser;

/**
 * @author Menderes Fatih GUVEN
 */
public class XmlModel implements Serializable {
	private static final long serialVersionUID = 1863448535219779682L;

	private XmlModelObject root;

	XmlModel() {
	}

	public XmlModelObject getRoot() {
		return root;
	}

	public void setRoot(XmlModelObject root) {
		this.root = root;
	}
	
	@Override
	public String toString() {
		return String.valueOf(root);
	}
	
	public static XmlModel parse(File file) throws XmlException {
		return parse(XmlParser.fromFileToDocument(file));
	}

	public static XmlModel parse(InputStream in) throws XmlException {
		return parse(XmlParser.fromInputStreamToDocument(in));
	}
	
	public static XmlModel parse(Reader reader) throws XmlException {
		return parse(XmlParser.fromReaderToDocument(reader));
	}
	
	public static XmlModel parse(Document document) throws XmlException {
		return parser.parse(document);
	}
	
	public Object decode(String text) throws XmlException {
		return decode(XmlParser.fromStringToDocument(text));
	}

	public Object decode(File file) throws XmlException {
		return decode(XmlParser.fromFileToDocument(file));
	}

	public Object decode(InputStream in) throws XmlException {
		return decode(XmlParser.fromInputStreamToDocument(in));
	}

	public Object decode(Reader reader) throws XmlException {
		return decode(XmlParser.fromReaderToDocument(reader));
	}

	public Object decode(Document document) throws XmlException {
		return decoder.decode(document, this);
	}
	
	public String encodeToString(Object object) throws XmlException {
		return XmlParser.fromDocumentToString(encode(object, this));
	}

	public void encodeToFile(Object object, File file) throws XmlException {
		XmlParser.fromDocumentToFile(encode(object, this), file);
	}

	public void encodeToOutputStream(Object object, OutputStream out) throws XmlException {
		XmlParser.fromDocumentToOutputStream(encode(object, this), out);
	}

	public void encodeToWriter(Object object, Writer writer) throws XmlException {
		XmlParser.fromDocumentToWriter(encode(object, this), writer);
	}

	public Document encode(Object object, XmlModel model) throws XmlException {
		return encoder.encode(object, model);
	}
	
	private static XmlModelParser parser = new DefaultXmlModelParser();
	public static XmlModelParser getParser() {
		return parser;
	}
	public static void setParser(XmlModelParser parser) {
		XmlModel.parser = parser;
	}
	
	private static XmlEncoder encoder = new DefaultXmlEncoder();
	public static XmlEncoder getEncoder() {
		return encoder;
	}
	public static void setEncoder(XmlEncoder encoder) {
		XmlModel.encoder = encoder;
	}
	
	private static XmlDecoder decoder = new DefaultXmlDecoder();
	public static XmlDecoder getDecoder() {
		return decoder;
	}
	public static void setDecoder(XmlDecoder decoder) {
		XmlModel.decoder = decoder;
	}
}
