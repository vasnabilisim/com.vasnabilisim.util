package com.vasnabilisim.xmlmodel;

import org.w3c.dom.Document;

import com.vasnabilisim.xml.XmlException;

/**
 * @author Menderes Fatih GUVEN
 */
public interface XmlModelParser {

	/**
	 * Parses xml model from given xml document.
	 * 
	 * @param document
	 * @return
	 * @throws XmlException
	 */
	XmlModel parse(Document document) throws XmlException;
}
