package com.vasnabilisim.xmlmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

/**
 * @author Menderes Fatih GUVEN
 */
public class XmlModelObject extends XmlModelNode implements Serializable {
	private static final long serialVersionUID = 747118840011266278L;
	
	private XmlModelObject baseObjectModel;
	private List<XmlModelObject> subObjectModelList = new ArrayList<>();

	private Class<?> instanceType;
	private List<XmlModelProperty> propertyList = new ArrayList<>();
	private List<XmlModelObject> objectList = new ArrayList<>();
	
	/**
	 * Set all fields of the object before appending to its parent.
	 */
	private boolean fullConstructBeforeAppend = true;
	
	private TreeMap<String, XmlModelNode> nodeMap = new TreeMap<>();
	
	public XmlModelObject() {
	}
	
	public XmlModelObject(XmlModelObject baseObjectModel, String xmlName, String objectName, Class<?> type, Multiplicity multiplicity) {
		super(xmlName, objectName, type, multiplicity);
		this.baseObjectModel = baseObjectModel;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append("<");
		if(baseObjectModel != null)
			builder.append("inherit=").append(baseObjectModel.xmlName).append(", ");
		builder.append("xml-name=").append(xmlName).append(", ");
		builder.append("object-name=").append(objectName).append(", ");
		builder.append("multiplicity=").append(multiplicity).append(", ");
		builder.append("type=").append(type.getName()).append(", ");
		builder.append("class=").append(instanceType.getName());
		builder.append(">");
		return builder.toString();
	}
	
	public XmlModelObject getBaseObjectModel() {
		return baseObjectModel;
	}
	
	public List<XmlModelObject> getSubObjectModelList() {
		return Collections.unmodifiableList(subObjectModelList);
	}
	
	public void addSubObjectModel(XmlModelObject subObjectModel) {
		subObjectModel.baseObjectModel = this;
		subObjectModelList.add(subObjectModel);
	}

	public Class<?> getInterfaceType() {
		return type;
	}
	
	public void setInterfaceType(Class<?> type) {
		this.type = type;
	}
	
	public Class<?> getInstanceType() {
		return instanceType;
	}
	
	public void setInstanceType(Class<?> instanceType) {
		this.instanceType = instanceType;
	}
	
	public List<XmlModelProperty> getPropertyList() {
		return Collections.unmodifiableList(propertyList);
	}
	
	public void addProperty(XmlModelProperty property) {
		propertyList.add(property);
		nodeMap.put(property.xmlName, property);
		property.setParent(this);
	}
	
	public List<XmlModelObject> getObjectList() {
		return Collections.unmodifiableList(objectList);
	}
	
	public void addObject(XmlModelObject object) {
		objectList.add(object);
		nodeMap.put(object.xmlName, object);
		object.setParent(this);
	}
	
	public boolean getFullConstructBeforeAppend() {
		return fullConstructBeforeAppend;
	}
	
	public void setFullConstructBeforeAppend(boolean fullConstructBeforeAppend) {
		this.fullConstructBeforeAppend = fullConstructBeforeAppend;
	}
	
	public XmlModelNode findNode(String nodeXmlName) {
		XmlModelNode xmlModelNode = nodeMap.get(nodeXmlName);
		if(xmlModelNode != null)
			return xmlModelNode;
		if(baseObjectModel != null) {
			xmlModelNode = baseObjectModel.findNode(nodeXmlName);
			if(xmlModelNode != null)
				return xmlModelNode;
		}
		return findNodeInSubObjects(this, nodeXmlName);
	}
	
	protected static XmlModelNode findNodeInSubObjects(XmlModelObject xmlModelObject, String nodeXmlName) {
		for(XmlModelObject childXmlModelObject : xmlModelObject.objectList) {
			if(childXmlModelObject.subObjectModelList.isEmpty())
				continue;
			for(XmlModelObject subXmlModelObject : childXmlModelObject.subObjectModelList)
				if(nodeXmlName.equals(subXmlModelObject.xmlName))
					return subXmlModelObject;
		}
		for(XmlModelObject childXmlModelObject : xmlModelObject.objectList) {
			XmlModelNode xmlModelNode = findNodeInSubObjects(childXmlModelObject, nodeXmlName);
			if(xmlModelNode != null)
				return xmlModelNode;
		}
		return null;
	}
}
