package com.vasnabilisim.xmlmodel;

import org.w3c.dom.Document;

import com.vasnabilisim.xml.XmlException;

/**
 * @author Menderes Fatih GUVEN
 */
public interface XmlEncoder {

	/**
	 * Creates and returns an xml document from given object using given model.
	 * 
	 * @param object
	 * @param model
	 * @return
	 * @throws XmlException
	 */
	Document encode(Object object, XmlModel model) throws XmlException;
}
