package com.vasnabilisim.xmlmodel;

import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.xml.XmlException;
import com.vasnabilisim.xml.XmlParser;

/**
 * @author Menderes Fatih GUVEN
 */
public class DefaultXmlModelParser implements XmlModelParser {

	@Override
	public XmlModel parse(Document document) throws XmlException {
		Element rootElement = document.getDocumentElement();
		Map<String, XmlModelObject> modelObjectMap = new TreeMap<String, XmlModelObject>();
		XmlModelObject rootModelElement = parse(rootElement, modelObjectMap);
		XmlModel model = new XmlModel();
		model.setRoot(rootModelElement);
		return model;
	}

	private XmlModelObject parse(Element parentElement, Map<String, XmlModelObject> modelObjectMap) throws XmlException {
		String xmlName = parentElement.getAttribute("xml-name");
		String objectName = parentElement.getAttribute("object-name");
		String typeName = parentElement.getAttribute("type");
		String objectKey = xmlName + "&" + objectName /*+ "&" + typeName*/;
		XmlModelObject parentModelObject = modelObjectMap.get(objectKey);
		if (parentModelObject != null)
			return parentModelObject;
		parentModelObject = new XmlModelObject();
		if(parentElement.hasAttribute("inherits")) {
			String baseXmlName = parentElement.getAttribute("inherits");
			String baseObjectKey = baseXmlName + "&" + objectName /*+ "&" + typeName*/;
			XmlModelObject baseModelObject = modelObjectMap.get(baseObjectKey);
			if(baseModelObject != null)
				baseModelObject.addSubObjectModel(parentModelObject);
		}
		modelObjectMap.put(objectKey, parentModelObject);
		parentModelObject.setXmlName(xmlName);
		parentModelObject.setObjectName(objectName);
		try {
			//TODO Use class loader
			parentModelObject.setInterfaceType(Class.forName(typeName));
		} catch (ClassNotFoundException e) {
			throw new XmlException(String.format("Unable to find class %s.", typeName), e);
		}
		if(parentElement.hasAttribute("class")) {
			try {
				//Use class loader
				parentModelObject.setInstanceType(Class.forName(parentElement.getAttribute("class")));
			} catch (ClassNotFoundException e) {
				throw new XmlException(String.format("Unable to find class %s.", parentElement.getAttribute("class")), e);
			}
		}
		else {
			parentModelObject.setInstanceType(parentModelObject.getInterfaceType());
		}
		String multiplicity = parentElement.getAttribute("multiplicity");
		try {
			parentModelObject.setMultiplicity(Enum.valueOf(Multiplicity.class, multiplicity));
		} catch (IllegalArgumentException e) {
			throw new XmlException(String.format("Unsported multiplicity %s.", multiplicity), e);
		}

		for (Element attributeElement : XmlParser.findChildElements(parentElement, "property")) { 
			XmlModelProperty modelProperty = new XmlModelProperty();
			modelProperty.setXmlName(attributeElement.getAttribute("xml-name"));
			modelProperty.setObjectName(attributeElement.getAttribute("object-name"));
			try {
				//Use class loader
				modelProperty.setType(Class.forName(attributeElement.getAttribute("type")));
			} catch (ClassNotFoundException e) {
				throw new XmlException(String.format("Unable to find class %s.", attributeElement.getAttribute("type")), e);
			}
			multiplicity = attributeElement.getAttribute("multiplicity");
			if(multiplicity != null && !multiplicity.isEmpty()) {
				try {
					modelProperty.setMultiplicity(Enum.valueOf(Multiplicity.class, multiplicity));
				} catch (IllegalArgumentException e) {
					throw new XmlException(String.format("Unsported multiplicity %s.", multiplicity), e);
				}
			}
			parentModelObject.addProperty(modelProperty);
		}

		for (Element element : XmlParser.findChildElements(parentElement, "object")) { 
			XmlModelObject modelElement = parse(element, modelObjectMap);
			parentModelObject.addObject(modelElement);
		}

		return parentModelObject;
	}
}
