package com.vasnabilisim.xmlmodel;

import org.w3c.dom.Document;

import com.vasnabilisim.xml.XmlException;

/**
 * @author Menderes Fatih GUVEN
 */
public interface XmlDecoder {

	/**
	 * Puts a reference object to parser with given key.
	 * @param name
	 * @param ref
	 * @return
	 */
	Object putRef(String name, Object ref);

	/**
	 * Creates and returns an object from given document using given model.
	 * 
	 * @param document
	 * @param model
	 * @return
	 * @throws XmlException
	 */
	Object decode(Document document, XmlModel model) throws XmlException;
}
