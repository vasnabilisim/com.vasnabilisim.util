package com.vasnabilisim.xmlmodel;

import java.io.Serializable;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class XmlModelNode implements Serializable {
	private static final long serialVersionUID = -6351608825663263261L;

	protected XmlModelObject parentObjectModel;
	protected String xmlName;
	protected String objectName;
	protected Class<?> type;
	protected Multiplicity multiplicity;
	
	public XmlModelNode() {
	}
	
	public XmlModelNode(String xmlName) {
		this(xmlName, null, Multiplicity.Single);
	}
	
	public XmlModelNode(String xmlName, String objectName) {
		this(xmlName, objectName, Multiplicity.Single);
	}
	
	public XmlModelNode(String xmlName, String objectName, Multiplicity multiplicity) {
		this.xmlName = xmlName;
		this.objectName = objectName;
		this.multiplicity = multiplicity;
	}
	
	public XmlModelNode(String xmlName, String objectName, Class<?> type, Multiplicity multiplicity) {
		this.xmlName = xmlName;
		this.objectName = objectName;
		this.type = type;
		this.multiplicity = multiplicity;
	}
	
	public XmlModelObject getParent() {
		return parentObjectModel;
	}
	
	public void setParent(XmlModelObject parentObjectModel) {
		this.parentObjectModel = parentObjectModel;
	}
	
	public String getXmlName() {
		return xmlName;
	}
	
	public void setXmlName(String xmlName) {
		this.xmlName = xmlName;
	}
	
	public String getObjectName() {
		return objectName;
	}
	
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	
	public Class<?> getType() {
		return type;
	}
	
	public void setType(Class<?> type) {
		this.type = type;
	}
	
	public Multiplicity getMultiplicity() {
		return multiplicity;
	}
	
	public void setMultiplicity(Multiplicity multiplicity) {
		this.multiplicity = multiplicity;
	}
}
