package com.vasnabilisim.argument;

import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import com.vasnabilisim.util.ArrayIterator;
import com.vasnabilisim.xml.XmlException;
import com.vasnabilisim.xmlmodel.XmlModel;

/**
 * @author Menderes Fatih GUVEN
 */
public class ArgInfos {

	private TreeMap<String, ArgInfo> argMap;
	
	public ArgInfos() {
		argMap = new TreeMap<String, ArgInfo>();
	}
	
	@Override
	public String toString() {
		return argMap.values().toString();
	}
	
	public Collection<ArgInfo> getArgCollection() {
		return argMap.values();
	}
	
	public void addArg(ArgInfo param) {
		for(String name : param.getNameList())
			argMap.put(name, param);
	}
	
	public Map<String, Object> parse(String arg) {
		StringTokenizer tokenizer = new StringTokenizer(arg, " \t\r\n\b");
		String[] args = new String[tokenizer.countTokens()];
		int index = 0;
		while(tokenizer.hasMoreTokens())
			args[index++] = tokenizer.nextToken();
		return parse(args);
	}
	
	public Map<String, Object> parse(String[] args) throws IllegalArgumentException {
		TreeMap<String, Object> result = new TreeMap<>();
		ArrayIterator<String> iterator = new ArrayIterator<String>(args);
		ArrayList<ArgInfo> foundArgList = new ArrayList<ArgInfo>(argMap.size());
		while(iterator.hasNext()) {
			String argName = iterator.next();
			ArgInfo arg = argMap.get(argName);
			if(arg == null) {
				throw new IllegalArgumentException(String.format("Unknown argument [%s]", argName));
			}
			Object value = null;
			if(arg.hasValue)
				value = arg.type.toObject(iterator.next());
			for(String name : arg.nameList)
				result.put(name, value);
			foundArgList.add(arg);
		}
		for(ArgInfo arg : argMap.values()) {
			if(foundArgList.contains(arg))
				continue;
			if(arg.mandatory) {
				throw new IllegalArgumentException(String.format("Unable to find mandatory argument [%s]", arg.getNames()));
			}
			Object value = null;
			if(arg.getHasValue())
				value = arg.getDefaultValue();
			for(String name : arg.nameList)
				result.put(name, value);
		}
		return result;
	}
	
	public void printUsage(PrintWriter writer) {
		ArrayList<ArgInfo> foundArgList = new ArrayList<ArgInfo>(argMap.size());
		for(ArgInfo argInfo : argMap.values()) {
			if(foundArgList.contains(argInfo))
				continue;
			
			boolean comma = false;
			for(String name : argInfo.nameList) {
				if(comma)
					writer.print(" or ");
				writer.print(name);
				comma = true;
			}
			writer.print(" :\t");
			writer.println(argInfo.description);
			
			foundArgList.add(argInfo);
		}
	}
	
	public void printUsage(PrintStream writer) {
		ArrayList<ArgInfo> foundArgList = new ArrayList<ArgInfo>(argMap.size());
		writer.println("USAGE");
		for(ArgInfo argInfo : argMap.values()) {
			if(foundArgList.contains(argInfo))
				continue;
			
			boolean comma = false;
			for(String name : argInfo.nameList) {
				if(comma)
					writer.print(" or ");
				writer.print(name);
				comma = true;
			}
			writer.print(" :\t");
			writer.println(argInfo.description);
			
			foundArgList.add(argInfo);
		}
	}

	public static ArgInfos loadArgInfos(String fileName) throws XmlException {
		return (ArgInfos) XmlModel.parse(ArgInfos.class.getResourceAsStream("Args.xmlmodel")).decode(new File(fileName));
	}
}
