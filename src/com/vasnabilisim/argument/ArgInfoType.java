package com.vasnabilisim.argument;

import com.vasnabilisim.dataconverter.DataConverter;

/**
 * @author Menderes Fatih GUVEN
 */
public enum ArgInfoType {

	String(java.lang.String.class), 
	Character(java.lang.Character.class), 
	Integer(java.lang.Integer.class), 
	Long(java.lang.Long.class), 
	Float(java.lang.Float.class), 
	Double(java.lang.Double.class), 
	BigInteger(java.math.BigInteger.class), 
	BigDecimal(java.math.BigDecimal.class), 
	Boolean(java.lang.Boolean.class), 
	DateTime(java.time.LocalDateTime.class), 
	Date(java.time.LocalDate.class), 
	Time(java.time.LocalTime.class);
	
	Class<?> objectClass;
	ArgInfoType(Class<?> objectClass) {
		this.objectClass = objectClass;
	}
	
	public Object toObject(String stringValue) {
		return DataConverter.convertTo(stringValue, objectClass);
	}
}
