package com.vasnabilisim.argument;

import java.util.ArrayList;

/**
 * @author Menderes Fatih GUVEN
 */
public class ArgInfo {

	ArrayList<String> nameList;
	Boolean mandatory = Boolean.FALSE;
	String description = null;
	ArgInfoType type = ArgInfoType.String;
	Boolean hasValue = Boolean.FALSE;
	String defaultStringValue = null;
	Object defaultValue = null;
	String stringValue = null;
	Object value = null;

	public ArgInfo() {
		nameList = new ArrayList<String>(1);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append('<');
		builder.append("name:");
		for(String name : nameList)
			builder.append(' ').append(name);
		builder.append(", mandatory: ").append(mandatory);
		builder.append(", description: ").append(description);
		builder.append(", type: ").append(type);
		builder.append(", hasValue: ").append(hasValue);
		builder.append(", defaultValue: ").append(defaultStringValue);
		builder.append(", value: ").append(stringValue);
		builder.append('>');
		return super.toString();
	}

	public ArrayList<String> getNameList() {
		return nameList;
	}
	
	public void addName(String name) {
		nameList.add(name);
	}
	
	public String getNames() {
		StringBuilder builder = new StringBuilder(100);
		for(String name : nameList) {
			if(builder.length() > 0)
				builder.append(' ');
			builder.append(name);
		}
		return builder.toString();
		
	}
	
	public Boolean getMandatory() {
		return mandatory;
	}
	
	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public ArgInfoType getType() {
		return type;
	}
	
	public void setType(ArgInfoType type) {
		this.type = type;
	}
	
	public Boolean getHasValue() {
		return hasValue;
	}
	
	public void setHasValue(Boolean hasValue) {
		this.hasValue = hasValue;
	}
	
	public String getDefaultStringValue() {
		return defaultStringValue;
	}
	
	public void setDefaultStringValue(String defaultStringValue) {
		this.defaultStringValue = defaultStringValue;
	}
	
	public Object getDefaultValue() {
		if(defaultValue == null && defaultStringValue != null)
			defaultValue = type.toObject(defaultStringValue);
		return defaultValue;
	}
	
	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	public String getStringValue() {
		return stringValue;
	}
	
	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
	
	public Object getValue() {
		if(value == null && stringValue != null)
			value = type.toObject(stringValue);
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
}
