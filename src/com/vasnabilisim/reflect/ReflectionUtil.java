package com.vasnabilisim.reflect;

import java.lang.reflect.Method;

/**
 * @author Menderes Fatih GUVEN
 */
public final class ReflectionUtil {

	private ReflectionUtil() {
	}
	
	public static <C> Method method(Class<? extends C> type, String methodName) throws ReflectionException {
		try {
			return type.getMethod(methodName);
		} catch (Exception e) {
			throw new ReflectionException(e);
		}
	}
	
	public static <C, P0> Method method(
			Class<? extends C> type, 
			String methodName, 
			Class<? extends P0> param0Type) throws ReflectionException {
		try {
			return type.getMethod(methodName, param0Type);
		} catch (Exception e) {
			throw new ReflectionException(e);
		}
	}
	
	public static <C, P0, P1> Method method(
			Class<? extends C> type, 
			String methodName, 
			Class<? extends P0> param0Type, 
			Class<? extends P1> param1Type) throws ReflectionException {
		try {
			return type.getMethod(methodName, param0Type, param1Type);
		} catch (Exception e) {
			throw new ReflectionException(e);
		}
	}
	
	public static <C, P0, P1, P2> Method method(
			Class<? extends C> type, 
			String methodName, 
			Class<? extends P0> param0Type, 
			Class<? extends P1> param1Type, 
			Class<? extends P2> param2Type) throws ReflectionException {
		try {
			return type.getMethod(methodName, param0Type, param1Type, param2Type);
		} catch (Exception e) {
			throw new ReflectionException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <C, R> R invoke(Method method, Class<R> returnType, C instance) throws ReflectionException {
		try {
			Object result = method.invoke(instance);
			if(result == null)
				return null;
			if(returnType != null)
				return returnType.cast(result);
			return (R)result;
		} catch (Exception e) {
			throw new ReflectionException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <C, R, P0> R invoke(
			Method method, 
			Class<R> returnType, 
			C instance, 
			P0 param0) throws ReflectionException {
		try {
			Object result = method.invoke(instance, param0);
			if(result == null)
				return null;
			if(returnType != null)
				return returnType.cast(result);
			return (R)result;
		} catch (Exception e) {
			throw new ReflectionException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public static <C, R, P0, P1> R invoke(
			Method method, 
			Class<R> returnType, 
			C instance, 
			P0 param0, 
			P0 param1) throws ReflectionException {
		try {
			Object result = method.invoke(instance, param0, param1);
			if(result == null)
				return null;
			if(returnType != null)
				return returnType.cast(result);
			return (R)result;
		} catch (Exception e) {
			throw new ReflectionException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public static <C, R, P0, P1, P2> R invoke(
			Method method, 
			Class<R> returnType, 
			C instance, 
			P0 param0, 
			P0 param1, 
			P0 param2) throws ReflectionException {
		try {
			Object result = method.invoke(instance, param0, param1, param2);
			if(result == null)
				return null;
			if(returnType != null)
				return returnType.cast(result);
			return (R)result;
		} catch (Exception e) {
			throw new ReflectionException(e);
		}
	}

	public static <C, R> R invoke(
			Class<? extends C> type, 
			Class<R> returnType, 
			String methodName, 
			C instance) throws ReflectionException 
	{
		return invoke(method(type, methodName), returnType, instance);
	}

	public static <C, R, P0> R invoke(
			Class<? extends C> type, 
			Class<R> returnType, 
			String methodName, 
			Class<? extends P0> param0Type, 
			C instance, 
			P0 param0) throws ReflectionException 
	{
		return invoke(method(type, methodName, param0Type), returnType, instance, param0);
	}

	public static <C, R, P0, P1> R invoke(
			Class<? extends C> type, 
			Class<R> returnType, 
			String methodName, 
			Class<? extends P0> param0Type, 
			Class<? extends P1> param1Type, 
			C instance, 
			P0 param0, 
			P1 param1) throws ReflectionException 
	{
		return invoke(method(type, methodName, param0Type, param1Type), returnType, instance, param0, param1);
	}

	public static <C, R, P0, P1, P2> R invoke(
			Class<? extends C> type, 
			Class<R> returnType, 
			String methodName, 
			Class<? extends P0> param0Type, 
			Class<? extends P1> param1Type, 
			Class<? extends P2> param2Type, 
			C instance, 
			P0 param0, 
			P1 param1, 
			P2 param2) throws ReflectionException 
	{
		return invoke(method(type, methodName, param0Type, param1Type, param2Type), returnType, instance, param0, param1, param2);
	}
}
