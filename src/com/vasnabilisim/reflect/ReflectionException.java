package com.vasnabilisim.reflect;

/**
 * @author Menderes Fatih GUVEN
 */
public class ReflectionException extends Exception {
	private static final long serialVersionUID = 725961765976905640L;

	/**
	 * 
	 */
	public ReflectionException() {
	}

	/**
	 * @param message
	 */
	public ReflectionException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ReflectionException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ReflectionException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ReflectionException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
