package com.vasnabilisim.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @author Menderes Fatih GUVEN
 */
public class XmlParser {

	/**
	 * Default constructor.
	 */
	public XmlParser() {
	}

	/**
	 * Returns a new document.
	 * 
	 * @return
	 * @throws XmlException
	 */
	public static Document newDocument() throws XmlException {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		} catch (ParserConfigurationException e) {
			throw new XmlException("Invalid XML Parser Configuration. Unable to create XML document builder.", e);
		}
	}

	/**
	 * Creates and returns a xml document from given text.
	 * 
	 * @param text
	 * @return
	 * @throws XmlException
	 */
	public static Document fromStringToDocument(String text) throws XmlException {
		StringReader sr = new StringReader(text);
		return fromReaderToDocument(sr);
	}

	/**
	 * Creates and returns a xml document from given file.
	 * 
	 * @param file
	 * @return
	 * @throws XmlException
	 */
	public static Document fromFileToDocument(File file) throws XmlException {
		FileInputStream in;
		try {
			in = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			throw new XmlException(String.format("Unable to read file %s.", file.getPath()), e);
		}
		return fromInputStreamToDocument(in);
	}

	/**
	 * Creates and returns a xml document from given input stream.
	 * 
	 * @param in
	 * @return
	 * @throws XmlException
	 */
	public static Document fromInputStreamToDocument(InputStream in) throws XmlException {
		InputStreamReader reader;
		try {
			reader = new InputStreamReader(in, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new XmlException("Unsupported encoding utf-8.", e);
		}
		return fromReaderToDocument(reader);
	}


	/**
	 * Creates and returns a xml document from given reader.
	 * 
	 * @param reader
	 * @return
	 * @throws XmlException
	 */
	public static Document fromReaderToDocument(Reader reader) throws XmlException {
		InputSource inputsource = new InputSource(reader);
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputsource);
		} catch (ParserConfigurationException e) {
			throw new XmlException("Invalid XML Parser Configuration. Unable to create XML document builder.", e);
		} catch (SAXException | IOException e) {
			throw new XmlException("Unable to parse source.", e);
		}
	}

	/**
	 * Returns string representation of given xml document.
	 * 
	 * @param document
	 * @return
	 * @throws XmlException
	 */
	public static String fromDocumentToString(Document document) throws XmlException {
		StringWriter sw = new StringWriter();
		fromDocumentToWriter(document, sw);
		return sw.toString();
	}

	/**
	 * Flushes given xml to given file.
	 * 
	 * @param document
	 * @param file
	 * @throws XmlException
	 */
	public static void fromDocumentToFile(Document document, File file) throws XmlException {
		FileOutputStream out;
		try {
			out = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			throw new XmlException(String.format("Unable to create or update file %s.", file.getPath()), e);
		}
		fromDocumentToOutputStream(document, out);
	}

	/**
	 * Flushes given xml to given output stream.
	 * 
	 * @param document
	 * @param out
	 * @throws XmlException
	 */
	public static void fromDocumentToOutputStream(Document document, OutputStream out) throws XmlException {
		OutputStreamWriter writer;
		try {
			writer = new OutputStreamWriter(out, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new XmlException("Unsupported encoding utf-8.", e);
		}
		fromDocumentToWriter(document, writer);
	}

	/**
	 * Flushes given xml to given writer.
	 * 
	 * @param document
	 * @param writer
	 * @throws XmlException
	 */
	public static void fromDocumentToWriter(Document document, Writer writer) throws XmlException {
		if (document == null)
			return;

		TransformerFactory transformerFactory = null;
		Transformer transformer = null;
		try {
			transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute("indent-number", new Integer(4));
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException | TransformerFactoryConfigurationError e) {
			throw new XmlException("Invalid XML Transformer Configuration. Unable to create XML transformer builder.", e);
		}

		StreamResult result = new StreamResult(writer);

		try {
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(new DOMSource(document), result);
		} catch (TransformerException e) {
			throw new XmlException("Unable to transform xml document.", e);
		}
	}

	/**
	 * Returns a list of elements which is child to given parent element.
	 * 
	 * @param parentElement
	 * @return
	 */
	public static List<Element> allChildElements(Element parentElement) {
		List<Element> childElementList = new LinkedList<Element>();
		for (Node node = parentElement.getFirstChild(); node != null; node = node.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE)
				childElementList.add((Element) node);
		}
		return childElementList;
	}

	/**
	 * Returns a list of elements which has given name and child to given parent element.
	 * 
	 * @param parentElement
	 * @param childElementName
	 * @return
	 */
	public static List<Element> findChildElements(Element parentElement, String childElementName) {
		List<Element> childElementList = new LinkedList<Element>();
		findAndFillChildElements(childElementList, parentElement, childElementName);
		return childElementList;
	}

	/**
	 * Fills the given list with elements which are child to given parent element and have given child element name.
	 * 
	 * @param childElementList
	 * @param parentElement
	 * @param childElementName
	 */
	public static void findAndFillChildElements(List<Element> childElementList, Element parentElement, String childElementName) {
		for (Node node = parentElement.getFirstChild(); node != null; node = node.getNextSibling()) {
			if (node.getNodeType() != Node.ELEMENT_NODE)
				continue;
			if (!node.getNodeName().equals(childElementName))
				continue;
			childElementList.add((Element) node);
		}
	}

	/**
	 * Returns a list of elements which have name starting with given name and child to given parent element.
	 * 
	 * @param parentElement
	 * @param childElementName
	 * @return
	 */
	public static List<Element> findChildElementsStartsWith(Element parentElement, String childElementName) {
		List<Element> childElementList = new LinkedList<Element>();
		findAndFillChildElementsStartsWith(childElementList, parentElement, childElementName);
		return childElementList;
	}

	/**
	 * Fills the given list with elements which are child to given parent element and have name starting with given name.
	 * 
	 * @param childElementList
	 * @param parentElement
	 * @param childElementName
	 */
	public static void findAndFillChildElementsStartsWith(List<Element> childElementList, Element parentElement, String childElementName) {
		for (Node node = parentElement.getFirstChild(); node != null; node = node.getNextSibling()) {
			if (node.getNodeType() != Node.ELEMENT_NODE)
				continue;
			if (!node.getNodeName().startsWith(childElementName))
				continue;
			childElementList.add((Element) node);
		}
	}

	/**
	 * Returns the first element which has given name and child to given parent element.
	 * 
	 * @param parentElement
	 * @param childElementName
	 * @return
	 */
	public static Element findChildElement(Element parentElement, String childElementName) {
		for (Node node = parentElement.getFirstChild(); node != null; node = node.getNextSibling()) {
			if (node.getNodeType() != Node.ELEMENT_NODE)
				continue;
			if (node.getNodeName().equals(childElementName))
				return (Element) node;
		}
		return null;
	}

	/**
	 * Returns the first element which is child to given parent element.
	 * 
	 * @param parentElement
	 * @return
	 */
	public static Element firstChildElement(Element parentElement) {
		for (Node node = parentElement.getFirstChild(); node != null; node = node.getNextSibling()) {
			if (node.getNodeType() != Node.ELEMENT_NODE)
				continue;
			return (Element) node;
		}
		return null;
	}

	/**
	 * Return the text content of given element.
	 * 
	 * @param element
	 * @return
	 */
	public static String getElementText(Element element) {
		return getElementText(element, null);
	}

	/**
	 * Return the text content of given element. 
	 * Returns given defaultValue if content is empty or null.
	 * 
	 * @param element
	 * @param defaultValue
	 * @return
	 */
	public static String getElementText(Element element, String defaultValue) {
		String text = element.getTextContent();
		return text == null || text.length() == 0 ? defaultValue : text;
	}

	/**
	 * Return the text content of given element as integer. 
	 * Returns given defaultValue if content is empty or null.
	 * 
	 * @param element
	 * @param defaultValue
	 * @return
	 */
	public static int getElementTextAsInt(Element element, int defaultValue) {
		String text = getElementText(element);
		try {
			return text == null ? defaultValue : Integer.parseInt(text);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	/**
	 * Return the text content of given element as float. 
	 * Returns given defaultValue if content is empty or null.
	 * 
	 * @param element
	 * @param defaultValue
	 * @return
	 */
	public static float getElementTextAsFloat(Element element, float defaultValue) {
		String text = getElementText(element);
		try {
			return text == null ? defaultValue : Float.parseFloat(text);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	/**
	 * Return the text content of given element as enum of given enumClass. 
	 * Returns given defaultValue if content is empty or null.
	 * 
	 * @param element
	 * @param enumClass
	 * @param defaultValue
	 * @return
	 */
	public static <E extends Enum<E>> E getElementTextAsEnum(Element element, Class<E> enumClass, E defaultValue) {
		String text = getElementText(element);
		try {
			return text == null ? defaultValue : Enum.valueOf(enumClass, text);
		} catch (Exception e) {
			return defaultValue;
		}
	}
	
	/**
	 * Returns all attributes of given element.
	 * 
	 * @param element
	 * @return
	 */
	public static List<Attr> allAttributes(Element element) {
		NamedNodeMap attributeMap = element.getAttributes();
		ArrayList<Attr> attributes = new ArrayList<>(attributeMap.getLength());
		for(int index=0; index<attributeMap.getLength(); ++index)
			attributes.add((Attr)attributeMap.item(index));
		return attributes;
	}
	
	/**
	 * Returns the attribute of given element having given name.
	 * @param element
	 * @param attributeName
	 * @return
	 */
	public static Attr findAttribute(Element element, String attributeName) {
		return element.getAttributeNode(attributeName);
	}

	/**
	 * Returns the value of the attribute of the given element having given attribute name. 
	 * Return null if no such attribute exists.
	 * 
	 * @param element
	 * @param attributeName
	 * @return
	 */
	public static String getAtributeValue(Element element, String attributeName) {
		return getAtributeValue(findAttribute(element, attributeName));
	}

	/**
	 * Returns the given attribute value. 
	 * Return null if given attribute is null.
	 * 
	 * @param attr
	 * @return
	 */
	public static String getAtributeValue(Attr attr) {
		return attr == null ? null : attr.getValue();
	}

	/**
	 * Returns the value of the attribute of the given element having given attribute name. 
	 * Return given defaultValue if no such attribute exists.
	 * 
	 * @param element
	 * @param attributeName
	 * @param defaultValue
	 * @return
	 */
	public static String getAtributeValue(Element element, String attributeName, String defaultValue) {
		return getAtributeValue(findAttribute(element, attributeName), defaultValue);
	}

	/**
	 * Return the given attribute value. 
	 * Returns given defaultValue if attribute or value is null.
	 * 
	 * @param element
	 * @param defaultValue
	 * @return
	 */
	public static String getAtributeValue(Attr attr, String defaultValue) {
		String text = getAtributeValue(attr);
		try {
			return text == null ? defaultValue : text;
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	/**
	 * Returns the integer value of the attribute of the given element having given attribute name. 
	 * Return given defaultValue if no such attribute exists.
	 * 
	 * @param element
	 * @param attributeName
	 * @param defaultValue
	 * @return
	 */
	public static int getAtributeValueAsInt(Element element, String attributeName, int defaultValue) {
		return getAtributeValueAsInt(findAttribute(element, attributeName), defaultValue);
	}

	/**
	 * Return the given attribute value as integer. 
	 * Returns given defaultValue if attribute or value is null.
	 * 
	 * @param element
	 * @param defaultValue
	 * @return
	 */
	public static int getAtributeValueAsInt(Attr attr, int defaultValue) {
		String text = getAtributeValue(attr);
		try {
			return text == null ? defaultValue : Integer.parseInt(text);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	/**
	 * Returns the float value of the attribute of the given element having given attribute name. 
	 * Return given defaultValue if no such attribute exists.
	 * 
	 * @param element
	 * @param attributeName
	 * @param defaultValue
	 * @return
	 */
	public static float getAtributeValueAsFloat(Element element, String attributeName, float defaultValue) {
		return getAtributeValueAsFloat(findAttribute(element, attributeName), defaultValue);
	}

	/**
	 * Return the given attribute value as float. 
	 * Returns given defaultValue if attribute or value is null.
	 * 
	 * @param element
	 * @param defaultValue
	 * @return
	 */
	public static float getAtributeValueAsFloat(Attr attr, float defaultValue) {
		String text = getAtributeValue(attr);
		try {
			return text == null ? defaultValue : Float.parseFloat(text);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	/**
	 * Returns the enum value of the attribute of the given element having given attribute name. 
	 * Return given defaultValue if no such attribute exists.
	 * 
	 * @param element
	 * @param attributeName
	 * @param defaultValue
	 * @return
	 */
	public static <E extends Enum<E>> E  getAtributeValueAsEnum(Element element, String attributeName, Class<E> enumClass, E defaultValue) {
		return getAtributeValueAsEnum(findAttribute(element, attributeName), enumClass, defaultValue);
	}

	/**
	 * Return the value given attribute as enum of given enumClass. 
	 * Returns given defaultValue if attribute or value is null.
	 * 
	 * @param attr
	 * @param enumClass
	 * @param defaultValue
	 * @return
	 */
	public static <E extends Enum<E>> E getAtributeValueAsEnum(Attr attr, Class<E> enumClass, E defaultValue) {
		String text = getAtributeValue(attr);
		try {
			return text == null ? defaultValue : Enum.valueOf(enumClass, text);
		} catch (Exception e) {
			return defaultValue;
		}
	}
}
