package com.vasnabilisim.xml;

/**
 * @author Menderes Fatih GUVEN
 */
public class XmlException extends java.lang.Exception {
	private static final long serialVersionUID = 1234734116041359004L;

	/**
	 * Type constructor.
	 */
	public XmlException() {
		super();
	}
	
	/**
	 * Message constructor.
	 * @param message
	 */
	public XmlException(String message) {
		super(message);
	}
	
	/**
	 * Cause constructor.
	 * @param e
	 */
	public XmlException(Throwable e) {
		super(e);
	}
	
	/**
	 * Compound constructor.
	 * @param message
	 * @param e
	 */
	public XmlException(String message, Throwable e) {
		super(message, e);
	}
}
